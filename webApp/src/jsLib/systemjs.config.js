/**
* System configuration for Angular samples
* Adjust as necessary for your application needs.
*/

(function (global) {
    System.config({
        paths: {
            // paths serve as alias
            // 'npm:': 'node_modules/'      // used if files are not copied to prod folder
            'npm:': 'npm/'                  // prod folder, where all used js files should be copied to
        },
        // map tells the System loader where to look for things
        map: {
            // our app is within the app folder
            app: 'app',

            // angular bundles - to be used if files are hosted from 'node_modules'
            // '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
            // '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
            // '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
            // '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
            // '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
            // '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
            // '@angular/router': 'npm:@angular/router/bundles/router.umd.js',

            // angular bundles - to be used if files are hosted in prod folder 'npm'
            '@angular/core': 'npm:misc/core.umd.js',
            '@angular/common': 'npm:misc/common.umd.js',
            '@angular/compiler': 'npm:misc/compiler.umd.js',
            '@angular/platform-browser': 'npm:misc/platform-browser.umd.js',
            '@angular/platform-browser-dynamic': 'npm:misc/platform-browser-dynamic.umd.js',
            '@angular/http': 'npm:misc/http.umd.js',
            '@angular/router': 'npm:misc/router.umd.js',

            // other libraries
            //   Reactive Extensions RxJS library (http://reactivex.io/rxjs/):
            //   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            //   "RxJS is a library for composing asynchronous and event-based programs by using observable sequences. It provides
            //   one core type, the Observable, satellite types (Observer, Schedulers, Subjects) and operators inspired by
            //   Array#extras (map, filter, reduce, every, etc) to allow handling asynchronous events as collections.
            //   Think of RxJS as lodash for events.
            //   ReactiveX combines the Observer pattern with the Iterator pattern and functional programming with collections
            //   to fill the need for an ideal way of managing sequences of events."
            //   ref: http://reactivex.io/rxjs/manual/overview.html#introduction
            'rxjs': 'npm:rxjs',

            // raven-js (https://github.com/getsentry/raven-js)
            // - - - - - - - - - - - - - - - - - - - - - - - - -
            // raven-js is a javascript client for sentry.io
            'raven-js': 'npm:misc/raven.js'
        },
        // packages tells the System loader how to load when no filename and/or no extension
        packages: {
            app: {
                main: './main.js',
                defaultExtension: 'js'
            },
            rxjs: {
                defaultExtension: 'js'
            }
        }
    });
})(this);
