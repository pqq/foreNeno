var cssId = 'myCss';
if (!document.getElementById(cssId))
{
    var head  = document.getElementsByTagName('head')[0];
    var link  = document.createElement('link');
    link.id   = cssId;
    link.rel  = 'stylesheet';
    link.type = 'text/css';

    // dynamically set stylesheet
    if(location.hostname == "localhost"){
        link.href = '/app/css/bootstrap_yeti.css?@TIMESTAMP';                       // yeti

        // for testing-purposes, the next lines are useful:
        //link.href = 'https://bootswatch.com/cerulean/bootstrap.css';              // YES - not in use
        //link.href = 'https://bootswatch.com/cosmo/bootstrap.css';                 // YES - neurogenes.is
        //link.href = 'https://bootswatch.com/cyborg/bootstrap.css';                // BACKUP - works. fonts are not great though.
        //link.href = 'https://bootswatch.com/darkly/bootstrap.css';                // NO - needs dark background
        //link.href = 'https://bootswatch.com/flatly/bootstrap.css';                // BACKUP - works. a tad difficult link colour for colourblind people like myself
        //link.href = 'https://bootswatch.com/journal/bootstrap.css';               // BACKUP - works. a more old fashion font.
        //link.href = 'https://bootswatch.com/lumen/bootstrap.css';                 // YES - not in use
        //link.href = 'https://bootswatch.com/paper/bootstrap.css';                 // YES - jernvilje.no
        //link.href = 'https://bootswatch.com/readable/bootstrap.css';              // BACKUP - works. readable.
        //link.href = 'https://bootswatch.com/sandstone/bootstrap.css';             // BACKUP - works. fresh colours.
        link.href = 'https://bootswatch.com/simplex/bootstrap.css';               // YES - thisworld.is
        //link.href = 'https://bootswatch.com/slate/bootstrap.css';                 // NO - needs dark background
        //link.href = 'https://bootswatch.com/solar/bootstrap.css';                 // UPDATE NEEDED - looks cool. link colour needs update.
        //link.href = 'https://bootswatch.com/spacelab/bootstrap.css';              // BACKUP - works. looks a tad dated
        //link.href = 'https://bootswatch.com/superhero/bootstrap.css';             // UPDATE NEEDED - looks cool, but dark background or change of font colours
        //link.href = 'https://bootswatch.com/united/bootstrap.css';                // YES - runners.no
        //link.href = 'https://bootswatch.com/yeti/bootstrap.css';                  // YES - foreneno.com & localhost

    } else if ( location.hostname.indexOf('foreneno.com') > -1 ) {
        link.href = '/app/css/bootstrap_yeti.css?@TIMESTAMP';                       // yeti
    } else if ( location.hostname.indexOf('neurogenes.is') > -1 ) {
        link.href = '/app/css/bootstrap_cosmo.css?@TIMESTAMP';                      // cosmo
    } else if ( location.hostname.indexOf('runners.no') > -1 ) {
        link.href = '/app/css/bootstrap_united.css?@TIMESTAMP';                     // united
    } else if ( location.hostname.indexOf('thisworld.is') > -1 ) {
        link.href = '/app/css/bootstrap_simplex.css?@TIMESTAMP';                    // simplex
    } else {
        link.href = '/app/css/bootstrap_yeti.css?@TIMESTAMP';                       // yeti
    }

    link.media = 'all';
    head.appendChild(link);
}
