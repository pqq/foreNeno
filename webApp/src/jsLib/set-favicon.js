(function() {
    var link = document.querySelector("link[rel*='icon']") || document.createElement('link');

    // ref: https://www.w3schools.com/tags/att_link_sizes.asp
    link.type = 'image/png';        // 'image/x-icon';
    link.rel = 'icon';
    link.sizes = 'any';             // '16x16 32x32';

    if(location.hostname == "localhost"){
        link.href = '/app/images/_favicon.png';
    } else if ( location.hostname.indexOf('foreneno.com') > -1 ) {
        link.href = '/app/images/_favicon.png';
    } else if ( location.hostname.indexOf('neurogenes.is') > -1 ) {
        link.href = '/app/images/neurogenesis_favicon.png';
    } else if ( location.hostname.indexOf('runners.no') > -1 ) {
        link.href = '/app/images/runnersno_favicon.png';
    } else if ( location.hostname.indexOf('thisworld.is') > -1 ) {
        link.href = '/app/images/thisworldis_favicon.png';
    } else {
        link.href = '/app/images/_favicon.png';
    }

    document.getElementsByTagName('head')[0].appendChild(link);

})();
