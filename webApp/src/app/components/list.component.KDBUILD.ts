import { Component, OnInit }        from '@angular/core';
import { ActivatedRoute }           from '@angular/router';
import                              'rxjs/add/operator/switchMap';

import { RpcService }               from '../services/rpc.service.KDBUILD';
import { LibraryService }           from '../services/library.service.KDBUILD';
import { HtmlService }              from '../services/html.service.KDBUILD';
import { TemplateService }          from '../services/template.service.KDBUILD';
import { Cfg }                      from '../classes/cfg.class.KDBUILD';
import { Record }                   from '../classes/record.class.KDBUILD';
import { Module }                   from '../classes/module.class.KDBUILD';


@Component({
      selector:     'foreneno-list'
    , templateUrl:  'app/templates/list.component.KDBUILD.html?@TIMESTAMP'
    , styleUrls:    ['app/css/list.component.css?@TIMESTAMP']
})
export class ListComponent implements OnInit {
    listName:       string;
    listType:       string;
    bundleId:       string;
    records:        Record[];
    mod:            Module;

    constructor(
          private rpcService: RpcService
        , private libraryService: LibraryService
        , private htmlService: HtmlService
        , private templateService: TemplateService
        , private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.onRouteChange();
    }

    // -----
    // valid list_type values:
    // - countries
    // - entries
    // - feeds
    // - maincats
    // - profiles
    // - subcats
    // -----
    onRouteChange() {
        this.route.params.subscribe(
              () => {
                  this.records      = Record[0];
                  this.mod          = new Module;
                  this.listType     = this.route.snapshot.params['list_type'];
                  this.listName     = this.route.snapshot.params['list_name'];
                  this.getRecords(this.route.snapshot.url.toString());
              }
            , err => console.error(err)
            , () => {}
        );
    }

    getRecords(url: string) {
        this.libraryService.globals.cfgSubject.subscribe(
            (cfg: Cfg) => {
                if (cfg) {
                    this.htmlService.setPageTitle(cfg.layout.siteTitle + ' ' + this.listName);
                    this.rpcService.rpcByUrl(url, cfg)
                    .subscribe(
                          (records: Record[]) => {
                              this.mod = this.libraryService.getModule(url, cfg);
                              this.records = records;
                              this.libraryService.globals.loadingStatus = 'loaded';
                          }
                        , (err: any) => {this.libraryService.errorMessage('list.component.getEntries(): ' + err);}
                        , () => {}
                    );
                }
            }
        );
    }

}
