import { Component, OnInit }        from '@angular/core';
import { ActivatedRoute }           from '@angular/router';

import { RpcService }               from '../services/rpc.service.KDBUILD';
import { LibraryService }           from '../services/library.service.KDBUILD';
import { HtmlService }              from '../services/html.service.KDBUILD';
import { TemplateService }          from '../services/template.service.KDBUILD';
import { Cfg }                      from '../classes/cfg.class.KDBUILD';
import { Record }                   from '../classes/record.class.KDBUILD';


@Component({
      selector:     'foreneno-static'
    , templateUrl:  'app/templates/page.component.KDBUILD.html?@TIMESTAMP'
    , styleUrls:    ['app/css/page.component.css?@TIMESTAMP']
})
export class StaticComponent implements OnInit {
    description:    string;
    body:           string;

    constructor(
          private rpcService: RpcService
        , private libraryService: LibraryService
        , private htmlService: HtmlService
        , private templateService: TemplateService
        , private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.onRouteChange();
    }

    onRouteChange() {
        this.route.params
            .subscribe(
                () => {
                    this.libraryService.globals.loadingStatus = 'loading';
                    this.getStaticContent();
                }
            );
    }

    getStaticContent() {
        let page_name   = this.route.snapshot.params['page_name'];
        let url         = this.route.snapshot.url.toString();
        url = url.toString().replace(/,/g, '/');

        this.libraryService.globals.cfgSubject.subscribe(
            (cfg: Cfg) => {
                if (cfg) {
                    this.htmlService.setPageTitle(cfg.layout.siteTitle + ' ' + page_name);
                    let mod = this.libraryService.getModule(url, cfg);
                    this.description = mod.description;
                    this.body = mod.body;
                    this.libraryService.globals.loadingStatus = 'loaded';
                }
            }
        );
    }

}
