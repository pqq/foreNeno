import { Component, OnInit }    from '@angular/core';

import { RpcService }           from '../services/rpc.service.KDBUILD';
import { LibraryService }       from '../services/library.service.KDBUILD';
import { HtmlService }          from '../services/html.service.KDBUILD';
import { About }                from '../classes/about.class.KDBUILD';
import { Cfg }                  from '../classes/cfg.class.KDBUILD';

@Component({
      selector:           'foreneno-about'
    , templateUrl:        'app/templates/about.component.KDBUILD.html?@TIMESTAMP'
    , styleUrls:          ['app/css/about.component.css?@TIMESTAMP']
})
export class AboutComponent implements OnInit {
    abouts: About[];

    constructor(
          private libraryService: LibraryService
        , private htmlService: HtmlService
        , private rpcService: RpcService
    ) {
    }

    ngOnInit() {
        this.getAbouts();
    }

    getAbouts() {
        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        // example of data structure, where "response" equals "result_set":
        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        // "result_set": [
        //     {
        //         "forenenotest": [
        //             {
        //                 "body": "this is the body, where the main text resides.",
        //                 "ingress": "this is an ingress",
        //                 "title": "this is a title"
        //             },
        //             {
        //                 "body": "and once again, this is the body, where the main text resides.",
        //                 "ingress": "and this is also an ingress",
        //                 "title": "this is a title too"
        //             }
        //         ]
        //     }
        // ]
        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        this.libraryService.globals.cfgSubject.subscribe(
            (cfg: Cfg) => {
                if (cfg) {
                    this.htmlService.setPageTitle(cfg.layout.siteTitle + ' ' + cfg.about.title);
                    let service = this.libraryService.getServiceId();

                    this.rpcService.rpc('bundle_id=core.about&method=list_entries_json', cfg)
                    .subscribe(
                        (response: About[]) => {
                            if (response) {
                                this.abouts = response[0][service];
                            }
                        }
                        , err => console.error(err)
                        , () => {}
                    );
                }
            }
        );

    }

}
