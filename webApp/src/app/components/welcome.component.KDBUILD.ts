import { Component, OnInit }    from '@angular/core';

import { RpcService }           from '../services/rpc.service.KDBUILD';
import { LibraryService }       from '../services/library.service.KDBUILD';
import { HtmlService }          from '../services/html.service.KDBUILD';
import { TemplateService }          from '../services/template.service.KDBUILD';
import { Cfg }                  from '../classes/cfg.class.KDBUILD';
import { Record }               from '../classes/record.class.KDBUILD';
import { Module }               from '../classes/module.class.KDBUILD';

@Component({
      selector:           'foreneno-welcome'
    , templateUrl:        './app/templates/welcome.component.KDBUILD.html?@TIMESTAMP'
    , styleUrls:          ['./app/css/welcome.component.css?@TIMESTAMP']
})
export class WelcomeComponent implements OnInit {
    records: Record[];
    mod:     Module;

    constructor(
          private rpcService: RpcService
        , private libraryService: LibraryService
        , private htmlService: HtmlService
        , private templateService: TemplateService
    ) {}

    ngOnInit() {
        this.getEntries();
    }

    getEntries() {

        // subscribe to the configuration, and trigger rpc when cfg is fully loaded
        this.libraryService.globals.cfgSubject.subscribe(
            (cfg: Cfg) => {
                if (cfg) {
                    // since the welcome component is no module we create a "fake" module with rpc parameters. this is done so we can find the bundle_id
                    this.mod = new Module;
                    this.mod.rpcParams = cfg.welcome.rpcParams;

                    this.htmlService.setPageTitle(cfg.layout.siteTitle);

                    // only execute rpc in case rpc parameters are given
                    if (this.libraryService.globals.cfg.welcome.rpcParams) {
                        // rpc() returns an observable that we subscribe to
                        // ref: http://reactivex.io/documentation/operators/subscribe.html
                        this.rpcService.rpc(this.libraryService.globals.cfg.welcome.rpcParams, cfg)
                        .subscribe(
                            // onNext:
                            // An Observable calls this method whenever the Observable emits an item.
                            // This method takes as a parameter the item emitted by the Observable.
                            (records: Record[]) => {this.records = records;}

                            // onError:
                            // An Observable calls this method to indicate that it has failed to
                            // generate the expected data or has encountered some other error.
                            // This stops the Observable and it will not make further calls to
                            // onNext or onCompleted. The onError method takes as its parameter
                            // an indication of what caused the error (sometimes an object like
                            // an Exception or Throwable, other times a simple string, depending
                            // on the implementation).
                            , (err: any) => {this.libraryService.errorMessage('welcome.component.getEntries(): ' + err);}

                            // onCompleted:
                            // An Observable calls this method after it has called onNext for
                            // the final time, if it has not encountered any errors.
                            , () => {console.log('foreNeno: "welcome to my world!"')}
                        );
                    // if no rpc parameters are given we set records to an empty array
                    } else {
                        this.records = [];
                    }
                }
            }
        );

    }

}
