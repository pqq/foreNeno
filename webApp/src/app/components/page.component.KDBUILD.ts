import { Component, OnInit }        from '@angular/core';
import { ActivatedRoute }           from '@angular/router';

import { RpcService }               from '../services/rpc.service.KDBUILD';
import { LibraryService }           from '../services/library.service.KDBUILD';
import { HtmlService }              from '../services/html.service.KDBUILD';
import { TemplateService }          from '../services/template.service.KDBUILD';
import { Cfg }                      from '../classes/cfg.class.KDBUILD';
import { Record }                   from '../classes/record.class.KDBUILD';
import { Module }                   from '../classes/module.class.KDBUILD';


@Component({
      selector:     'foreneno-page'
    , templateUrl:  'app/templates/page.component.KDBUILD.html?@TIMESTAMP'
    , styleUrls:    ['app/css/page.component.css?@TIMESTAMP']
})
export class PageComponent implements OnInit {
    pageType:       string;
    records:        Record[];
    mod:            Module;
    url:            string;

    constructor(
          private rpcService: RpcService
        , private libraryService: LibraryService
        , private htmlService: HtmlService
        , private templateService: TemplateService
        , private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.onRouteChange();
    }

    onRouteChange() {
        this.route.params
            .subscribe(
                () => {
                    this.getEntries();
                }
            );
    }

    // -----
    // valid page_type values:
    // - country
    // - entry
    // - feed
    // - maincat
    // - profile
    // - subcat
    // -----
    getEntries() {
        // reset previously loaded values, in case of route change within the same module
        this.records = Record[0];
        this.mod = new Module;

        // format of the url (where '/:bundle_id' is optional and is passed forward without being manipulated here)
        // - http://domain/page/:page_type/:id_name/:id_value/:bundle_id
        // examples:
        // - http://localhost:3000/page/entry/entry_id/3a9c6e7f37e2896d
        // - http://localhost:3000/page/entry/entry_id/3a9c6e7f37e2896d/bundle_id

        this.pageType   = this.route.snapshot.params['page_type'];
        let id_name     = this.route.snapshot.params['id_name'];
        let id_value    = this.route.snapshot.params['id_value'];
        let url         = this.route.snapshot.url.toString();

        url = url.toString().replace(/,/g, '/').replace(id_name, ':id_name');
        this.url = url;

        // we're doing a special last-entry-replace of the id_value. the reason for this is that in cases where the id_value equals the page_type
        // we would run into problems otherwise.
        // example:
        //      we have a main category with the name 'maincat'. that gives us the url:
        //      http://localhost:3000/page/maincat/feed_main_category_id/maincat
        //      if we did a normal replace the first occurrence would be replaces, so we would end up with the wrong url:
        //      http://localhost:3000/page/:id_value/:id_name/maincat
        //      doing a last-entry-replace we get the correct result:
        //      http://localhost:3000/page/maincat/:id_name/:id_value

        // in cases where name of bundle_id comes in conflict with maincat, subcat or feed name we need to make sure the replacement is done correctly
        // if there are 4 slashes the url contains a bundle_id, like: "page/maincat/:id_name/:id_value/blog"
        // if there are 3 slashes the url is without a bundle_id, like: "page/maincat/:id_name/:id_value"
        // hence, if 4 slashes then we can make a replacement with a trailing slash in the replacement expression
        // (for more details see issues #446 and #447)
        let isBundleUrl = this.libraryService.isBundleUrl(url);

        if (isBundleUrl) {
            url = this.libraryService.replaceLast(url, '/'+id_value+'/', '/'+':id_value'+'/');
        } else {
            url = this.libraryService.replaceLast(url, '/'+id_value, '/'+':id_value');
        }

        this.libraryService.globals.cfgSubject.subscribe(
            (cfg: Cfg) => {
                if (cfg) {
                    this.rpcService.rpcByUrl(url, cfg, id_name, id_value)
                    .subscribe(
                          (records: Record[]) => {
                              if (this.pageType == 'entry') {
                                  if (records[0]) {
                                      this.htmlService.setPageTitle(cfg.layout.siteTitle + ' ' + records[0].entry_title);
                                  } else {
                                      this.htmlService.setPageTitle(cfg.layout.siteTitle + ' n/a');
                                  }
                              } else {
                                  this.htmlService.setPageTitle(cfg.layout.siteTitle + ' ' + id_value);
                              }

                              this.mod = this.libraryService.getModule(url, cfg);
                              this.records = records;
                              this.libraryService.globals.loadingStatus = 'loaded';
                          }
                        , (err: any) => {this.libraryService.errorMessage('entrypage.component.getEntries(): ' + err);}
                        , () => {}
                    );
                }
            }
        );

    }

}
