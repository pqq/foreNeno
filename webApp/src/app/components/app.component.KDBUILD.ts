import { Component }        from '@angular/core';

import { LibraryService }   from '../services/library.service.KDBUILD';
import { HtmlService }      from '../services/html.service.KDBUILD';

@Component({
      selector:     'foreneno-app'
    , templateUrl:  './app/templates/app.component.KDBUILD.html?@TIMESTAMP'
    , styleUrls:    ['./app/css/app.component.css?@TIMESTAMP']
})
export class AppComponent {

    constructor(
          private libraryService: LibraryService
        , private htmlService: HtmlService
    ) {
        // trigger loading and setup of foreNeno
        libraryService.spawn();
    }

}
