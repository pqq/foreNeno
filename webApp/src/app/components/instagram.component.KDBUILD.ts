import { Component, OnInit }    from '@angular/core';
import { ActivatedRoute }       from '@angular/router';

import { LibraryService }       from '../services/library.service.KDBUILD';
import { HtmlService }          from '../services/html.service.KDBUILD';
import { Cfg }                  from '../classes/cfg.class.KDBUILD';

declare var Instafeed:any;

@Component({
      selector:     'foreneno-instagram'
    , templateUrl:  'app/templates/instagram.component.KDBUILD.html?@TIMESTAMP'
    , styleUrls:    ['app/css/instagram.component.css?@TIMESTAMP']
})
export class InstagramComponent implements OnInit {
    description: string;

    constructor(
          private libraryService: LibraryService
        , private htmlService: HtmlService
        , private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.onRouteChange();
    }

    onRouteChange() {
        this.route.params
            .subscribe(
                () => {
                    this.libraryService.globals.loadingStatus = 'loading';
                    this.onCfg(this.route.snapshot.url.toString());
                }
            );
    }

    onCfg(url: string) {
        this.libraryService.globals.cfgSubject.subscribe(
            (cfg: Cfg) => {
                if (cfg) {
                    this.htmlService.setPageTitle(cfg.layout.siteTitle + ' ' + url.replace(',', '/'));
                    let mod = this.libraryService.getModule(url, cfg);
                    this.description = mod.description;
                    let instaCommands = mod.instagram.parameters.split('!');
                    let instagramAccessToken = mod.instagram.accessToken;
                    let instagramClientId = mod.instagram.clientId;

                    this.doInsta(instaCommands, instagramAccessToken, instagramClientId);

                    this.libraryService.globals.loadingStatus = 'loaded';
                }
            }
        );
    }

    doInsta(instaCommands: string[], instagramAccessToken: string, instagramClientId: string) {

        let filterFunction = function (image: any) {
            if (image.type === 'video') {
              image.type_icon = '&#127909;';
            } else if (image.type === 'image') {
                image.type_icon = '&#128247;';
            } else {
              image.type_icon = image.type;
            }
            return true;
        };

        let postWidth : string = '0px';
        let postHeight : string = '0px';

        if (instaCommands[4] === 'thumbnail'){
            postWidth   = '170px';
            postHeight  = '100%';
        } else if (instaCommands[4] === 'low_resolution'){
            postWidth   = '340px';
            postHeight  = '100%';
        } else if (instaCommands[4] === 'standard_resolution'){
            postWidth   = '660px';
            postHeight  = '100%';
        }

        // by some to me unknown reason i do not manage to use styling from instaone.component.css in template...
        let templateString : string =
            '<div style="width: '+postWidth+'; height: '+postHeight+'; display: inline-block; padding: 10px; vertical-align: top; border-radius: 5px; border: 1px solid #e0ebeb; margin: 5px; cursor: pointer;" onclick="window.open(\'{{link}}\',\'mywindow\');" title="click to view on instagram">'
          + '<div style="background-color:; text-align: center; width: 100%; font-size: 110%; font-weight: 300;">{{model.type_icon}}</div>'
          + '<div style=""><a href="{{link}}" title="click to view on instagram" target="_blank"><img class="img-rounded" src="{{image}}" /></a></div>'
          + '<div style="background-color:; text-align: center; width: 100%; font-size: 110%; font-weight: 300;">&hearts; {{likes}} &#9998; {{comments}}</div>'
          + '<div><b>{{model.user.username}}:</b>&nbsp;{{caption}}</div>'
          + '<div style="color: #bbbbbb; font-size: smaller;"> {{location}}</div>'
          + '</div>';

          // reset the div for when directly switching in between several instagram modules
          document.getElementById('instagram').innerHTML = "";

        if ( instaCommands[0]==='user' ) {

            var userFeed = new Instafeed({
                  get:          'user'
                , accessToken:  instagramAccessToken
                , userId:       instaCommands[1]
                , sortBy:       instaCommands[2]
                , limit:        instaCommands[3]
                , resolution:   instaCommands[4]
                , filter:       filterFunction
                , template:     templateString
                , target:       'instagram'
            });
            userFeed.run();

        } else if ( instaCommands[0]==='tagged' ) {

            var hashtagFeed = new Instafeed({
                  get:          'tagged'
                , accessToken:  instagramAccessToken
                , clientId:     instagramClientId
                , tagName:      instaCommands[1]
                , sortBy:       instaCommands[2]
                , limit:        instaCommands[3]
                , resolution:   instaCommands[4]
                , filter:       filterFunction
                , template:     templateString
                , target:       'instagram'
            });
            hashtagFeed.run();

        }
    }
}
