import { Component, OnInit }    from '@angular/core';
import { ActivatedRoute }       from '@angular/router';

import { LibraryService }       from '../services/library.service.KDBUILD';
import { HtmlService }          from '../services/html.service.KDBUILD';
import { Cfg }                  from '../classes/cfg.class.KDBUILD';


@Component({
      selector:     'foreneno-iframe'
    , templateUrl:  'app/templates/iframe.component.KDBUILD.html?@TIMESTAMP'
    , styleUrls:    ['app/css/iframe.component.css?@TIMESTAMP']
})
export class IframeComponent implements OnInit {
    description: string;
    iframeSrc: string;

    constructor(
          private libraryService: LibraryService
        , private htmlService: HtmlService
        , private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.onRouteChange();
    }

    onRouteChange() {
        this.route.params
            .subscribe(
                () => {
                    this.libraryService.globals.loadingStatus = 'loading';
                    this.doIframe(this.route.snapshot.url.toString());
                }
            );
    }

    doIframe(url: string) {
        this.libraryService.globals.cfgSubject.subscribe(
            (cfg: Cfg) => {
                if (cfg) {
                    this.htmlService.setPageTitle(cfg.layout.siteTitle + ' ' + url.replace(',', '/'));
                    let mod = this.libraryService.getModule(url, cfg);
                    this.description = mod.description;
                    this.iframeSrc = mod.iframe.src;
                    this.libraryService.globals.loadingStatus = 'loaded';
                }
            }
        );
    }

}
