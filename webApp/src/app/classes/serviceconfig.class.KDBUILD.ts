import { Cfg } from '../classes/cfg.class.KDBUILD';

export class ServiceConfig {
    default:        Cfg;
    dev:            Cfg;
    beta:           Cfg;
    prod:           Cfg;
}
