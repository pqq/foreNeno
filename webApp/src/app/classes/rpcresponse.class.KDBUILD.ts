export class RpcResponse {
    bundle_id:      string;
    cache:          boolean;
    env_id:         string;
    hits:           number;
    load_time:      number;
    result_set:     any[];
    service_id:     string;
    stream_type:    string;
}
