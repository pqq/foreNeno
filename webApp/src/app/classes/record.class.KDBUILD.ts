export class Record {
    entry_content_html:             string;
    entry_content_text:             string;
    entry_id:                       string;
    entry_id_next:                  string;
    entry_id_previous:              string;
    entry_link:                     string;
    entry_number:                   number;
    entry_published_date:           string;
    entry_published_date_display:   string;
    entry_published_date_nerd:      string;
    entry_tags:                     string[];
    entry_title:                    string;
    feed_country_code:              string;
    feed_id:                        string;
    feed_main_category:             string;
    feed_main_category_id:          string;
    feed_profile:                   string;
    feed_profile_id:                string;
    feed_source:                    string;
    feed_source_id:                 string;
    feed_special_code:              string;
    feed_sub_category:              string;
    feed_sub_category_id:           string;
}
