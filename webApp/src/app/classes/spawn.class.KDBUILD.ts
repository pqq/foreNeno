export class Spawn {

    rpcBaseUrl: {       // base url/path to the rpc service, per environment, is stored here
        dev:    string; // development environment
        beta:   string; // beta environment
        prod:   string; // production environment
    }

    goBaseUrl: {        // base url/path to the go.py (jump to) script
        dev:    string;
        beta:   string;
        prod:   string;
    }

}
