import { ServiceConfig } from '../classes/serviceconfig.class.KDBUILD';

export class CfgResponse {
    atestservice: ServiceConfig;
    forenenocom: ServiceConfig;
    neurogenesis: ServiceConfig;
    runnersno: ServiceConfig;
    srmforeneno: ServiceConfig;
    thisworldis: ServiceConfig;

    constructor(cfgResponse: CfgResponse){
        this.atestservice   = cfgResponse.atestservice;
        this.forenenocom    = cfgResponse.forenenocom;
        this.neurogenesis   = cfgResponse.neurogenesis;
        this.runnersno      = cfgResponse.runnersno;
        this.srmforeneno    = cfgResponse.srmforeneno;
        this.thisworldis    = cfgResponse.thisworldis;
    }

    public get(serviceId: string) {
        if (serviceId === 'atestservice') {
            return this.atestservice;
        } else if (serviceId === 'forenenocom') {
            return this.forenenocom;
        } else if (serviceId === 'neurogenesis') {
            return this.neurogenesis;
        } else if (serviceId === 'runnersno') {
            return this.runnersno;
        } else if (serviceId === 'srmforeneno') {
            return this.srmforeneno;
        } else if (serviceId === 'thisworldis') {
            return this.thisworldis;
        } else {
            console.log('ERROR: cfgresponse.class.get(): unable to find configuration for service id "' + serviceId + '"');
        }
    }

}
