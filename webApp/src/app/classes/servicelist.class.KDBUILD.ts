export class ServiceList {

    // -----
    // data structure
    // -----
    public services:
    [
        {
            serviceId:  string;
            domain:     string;
        }
    ]

    // -----
    // when creating a new ServiceList object all available services are defined
    // -----
    constructor(){
        this.init('atestservice', 'localhost');         // atestservice - forenenocom - neurogenesis - runnersno - srmforeneno - thisworldis
        this.init('forenenocom', 'foreneno.com');
        this.init('neurogenesis', 'neurogenes.is');
        this.init('runnersno', 'runners.no');
        this.init('srmforeneno', 'srm.forene.no');
        this.init('thisworldis', 'thisworld.is');
    }

    // -----
    // either creates a new array, or push/add to an existing array
    // -----
    private init(serviceId: string, domain: string) {
        if (this.services) {
            this.services.push({serviceId, domain});
        } else {
            this.services = [{serviceId, domain}];
        }
    }

}
