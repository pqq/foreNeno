export class Module {

    title?:                 string;     // the title of the module that will be displayed in the top menu
    description?:           string;     // description to be presented at top of the module
    body?:                  string;     // the body party of a page that will be presented below the description
    path:                   string;     // relative path for the module - identical path has to be registered in 'app.routing.module'
    rpcParams?:             string;     // rpc parameters for selecting entries
    iframe?: {                          // iframe module
        src:                string;     // source/url of the content to present in the iframe
    }
    instagram?: {                       // instagram module
        description?:       string;     // description to be presented at top of the page
        userId:             string;     // user id
        clientId:           string;     // client id
        accessToken:        string;     // access token
        parameters:         string;     // instafeed.js listing parameters
    }

}
