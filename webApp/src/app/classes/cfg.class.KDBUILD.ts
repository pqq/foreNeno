import { Module } from '../classes/module.class.KDBUILD';

export class Cfg {
    constructor() { }

    rpc?: {                                     // rpc settings
        serviceId?:                 string;     // used as "service_id" when using rpc
        envId?:                     string;     // used as "env_id" when using rpc
        cache?:                     string;     // specifies whether or not caching is activated for the rpc calls. valid values are "true" and "false"
    }

    meta?: {                                    // html meta tag info
        author?:                    string;     // <meta name="author" content="">
        description?:               string;     // <meta name="description" content="">
        keywords?:                  string;     // <meta name="keywords" content="">
    }

    misc?: {                                    // miscellaneous values
        linkReferer?:               string;     // the value added to external links as a referer value 'http://some.external.link?ref=[linkReferer]'
    }

    layout?: {                                  // layout settings
        siteTitle?:                 string;     // the title of the site, displayed in the main menu and title of web page

        navbarClass?:               string;     // defines the class string (classes) used for the bootstrap navbar
        navbarLogo?:                string;     // defines the logo to be presented in the navbar
        paddingTop?:                string;     // defines the top padding of the foreNeno app
        mobileMenuView?:            string;     // defines if top menu for mobile is shown or not, by default

        goToSourceTitle?:           string;     // the text of the "go to source link" on all pages where this is relevant
        readMoreTitle?:             string;     // the title of the "read more link" on all pages where this is relevant
    }

    about?: {                                   // about module
            title?:                 string;     // the title of the about page - if empty no link will be displayed
    }

    welcome?: {                                 // the service's welcome module
        title?:                     string;     // menu title of welcome page - if empty no link will be displayed
        banner?:                    string;     // the filename of the site's banner
        logo?:                      string;     // the filename of the site's logo
        infoMessage?:               string;     // an info message to be presented on the welcome page
        heading?:                   string;     // top heading, on welcome page
        bottomLinkTitle?:           string;     // title of link at the bottom of entries list on the welcome page
        bottomLinkUrl?:             string;     // url (relative path) of link at the bottom of entries list on the welcome page
        rpcParams?:                 string;     // rpc parameters for fetching entries for the welcome page
    }

    modules?:                       Module[];     // the service's modules

}
