import { Spawn } from '../classes/spawn.class.KDBUILD';

export var SPAWN: Spawn =
{
      'rpcBaseUrl': {
            'dev':  'http://127.0.0.1:8080/foreNeno/cgi/rpc.py'
          , 'beta': 'rpc.py'
          , 'prod': 'rpc.py'
      }
      ,
      'goBaseUrl': {
          'dev':  'http://127.0.0.1:8080/foreNeno/cgi/go.py'
        , 'beta': 'go.py'
        , 'prod': 'go.py'
      }
};
