import { Injectable }                       from '@angular/core';
import { Title, Meta, MetaDefinition }      from '@angular/platform-browser';

/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    description:
        - generic (none foreNeno specific) html functions

    note:
        - HtmlService functions should never be called directly from any html
          templates, instead TemplateService is to be used in all cases.

    foreNeno dependencies:
        - NONE

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
@Injectable()
export class HtmlService {

    constructor(
          private title: Title
        , private meta: Meta
    ) {}

    // -----
    // html encode string
    // -----
    public encodeString(text:string) {
        let result = text;
        result = result.replace(/\'/g, '&rsquo;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return result;
    }

    // -----
    // shorten a string
    // -----
    public shortenString(text:string, length:number) {
        if (text.length > length) {
            return text.substring(0, length) + '...';
        } else {
            return text;
        }
    }

    // -----
    // returns the hostname
    // -----
    public getHostName() {
        return window.location.hostname;
    }

    // -----
    // get the first image in a html string
    // -----
    public getFirstImage(htmlCode:string) {
        // <img alt=\"GOPR9503_1450461256625_high.JPG\" src=\"http://1.bp.blogspot.com/-ZG8_M2DKVkQ/VnXJxT2vsSI/AAAAAAAA4wU/uxBYtiigRSQ/s1000/GOPR9503_1450461256625_high.JPG\" />
        let re      = /<img[^>]+src=\\?"?([^"\s]+)\\?"?[^>]*\/>/g;
        let results = re.exec(htmlCode);
        let img     = "";
        if (results) img = results[1];
        // remove "s72-c/" from the image urls - fix for blogger's reduced thumbnail sizes (https://github.com/klevstul/foreNeno/issues/393)
        img = img.replace('s72-c/','');
        return img;
    }

    // -----
    // returns the domain name part of a url
    // -----
    public getDomainName(url:string) {
        let domain = '';

        // http://news.google.com/news/url?sa=t&fd=R&ct2=us&usg=AFQjCNE4FYHOZ_BwWi94CZ7OgOUfYZNXhw&clid=c3a7d30bb8a4878e06b80cf16b898331&cid=52779948344016&ei=RM2zV7CiGYqpyAOOk4vwDQ&url=https://www.nrk.no/sport/toff-ol-debut-for-iuel-1.13090284
        if ( url.match('news.google.com') ) {
            url = url.split('&url=')[1];
        }

        // avoid showing the domain 'web.stagram.com' to the public, in the readmore links
        if ( url.match('web.stagram.com') ) {
            url = url.replace('web.stagram', 'instagram');
        }

        let url_array = url.replace('http://','').replace('https://','').replace('www.','').split(/[/?#]/);
        domain  = url_array[0];

        return domain;
    }

    // -----
    // scrolls to the top of the window. nifty if using a fixed menu bar.
    // -----
    public scrollToTop() {
        window.scrollTo(0,0);
    }

    // -----
    // set page title
    // -----
    public setPageTitle(title: string) {
        this.title.setTitle(title);
    }

    // -----
    // set meta tags
    // -----
    public setMetaTags(metaDefinition: MetaDefinition[]) {
        this.meta.addTags(metaDefinition);
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // misc help functions for templateService.prepareHtmlContent()
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    public removeLinks(htmlCode:string) {
        return htmlCode.replace(/<a[^>]*>(.*?)<\/a>/g, '$1');
    }

    public blankifyTarget(htmlCode:string) {
        return htmlCode = htmlCode.replace(/<a /g, '<a target="_blank" ');
    }

    public contentCleaner(htmlCode:string, specialCode:string) {
        // google news
        if (htmlCode.toLowerCase().indexOf('news.google.com') > -1) {
            htmlCode = this.removeLinks(htmlCode);
            htmlCode = htmlCode.replace(/and more/gi, '');                              // removing "and more" text
            htmlCode = htmlCode.replace(/\u00bb/gi, '');                                // removing double arrow
            htmlCode = htmlCode.replace(/all \d+ news articles/gi, '');                 // removing "all 55 news articles" info
            htmlCode = htmlCode.replace(/<td/gi, '<td style=\"padding-left:20px;\"');   // adding some space in between thubnail and text
            htmlCode = htmlCode.replace(/width=\"80/gi, 'width=\"120');                 // increasing width of td that holds thumbnail
        }

        // web.stagram.com
        if (htmlCode.toLowerCase().indexOf('web.stagram.com') > -1) {
            htmlCode = this.removeLinks(htmlCode);
        }

        // remove non-working google maps links (issue #189)
        htmlCode = htmlCode.replace(/<img alt="StaticMapService.GetMapImage/gi, '<img_removed alt="StaticMapService.GetMapImage');
        htmlCode = htmlCode.replace(/<div style="height: 256px; width: 256px;"><\/div>/gi, '');
        htmlCode = htmlCode.replace(/<img alt="" src="https:\/\/maps.googleapis.com\/maps\/vt?/gi, '<img_removed alt="" src="https:\/\/maps.googleapis.com\/maps\/vt?');

        // remove width and height settings (originally introduced due to issue #206, for images, but is done for all html code)
        htmlCode = htmlCode.replace(/ height=/gi, ' height_removed=');
        htmlCode = htmlCode.replace(/ width=/gi, ' width_removed=');

        // remove all css styles
        if ( !specialCode.match('asc=1') ) {
            htmlCode = htmlCode.replace(/ style=/gi, 'style_removed=');
        }

        return htmlCode;
    }

    public responsifyImages(htmlCode:string, cssClass:string) {
        return htmlCode = htmlCode.replace(/<img /g, '<img class="'+cssClass+'"');
    }

    public responsifyIframes(htmlCode:string) {
        // ref: http://stackoverflow.com/questions/25228056/responsive-iframe-using-bootstrap
        htmlCode = htmlCode.replace(/<iframe /g, '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item"');
        htmlCode = htmlCode.replace(/<\/iframe>/g, '<\/iframe></div>');
        return htmlCode;
    }

}
