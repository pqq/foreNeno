import { Injectable }       from '@angular/core';

import { LibraryService }   from '../services/library.service.KDBUILD';
import { HtmlService }      from '../services/html.service.KDBUILD';
import { Module }           from '../classes/module.class.KDBUILD';
import { Record }           from '../classes/record.class.KDBUILD';

/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    description:
        - help functions for html templates, called from html template files

    foreNeno dependencies:
        - LibraryService
        - HtmlService

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
@Injectable()
export class TemplateService {

    constructor(
          private libraryService: LibraryService
        , private htmlService: HtmlService
    ) {}

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // PUBLIC HELP FUNCTIONS
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // HtmlService redirect functions
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // -----
    // shorten a string
    // -----
    public shortenString(text:string, length:number) {
        return this.htmlService.shortenString(text, length);
    }

    // -----
    // html encode string
    // -----
    public encodeString(text:string) {
        return this.htmlService.encodeString(text);
    }

    // -----
    // returns the domain name part of a url
    // -----
    public getDomainName(url:string) {
        return this.htmlService.getDomainName(url);
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // other functions
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // -----
    // prepare text content for display
    // -----
    public prepareTextContent(textContent:string) {

        // feed43.com rss service
        textContent = textContent.replace(/-- delivered by feed43 service/gi, '');

        return textContent;
    }

    // -----
    // prepare html content for display
    // -----
    public prepareHtmlContent(htmlContent:string, specialCode:string) {
        let cssClass:string;

        if ( specialCode.match('el=0') ) {
            htmlContent = this.htmlService.removeLinks(htmlContent);
        }
        htmlContent = this.htmlService.blankifyTarget(htmlContent);
        htmlContent = this.htmlService.contentCleaner(htmlContent, specialCode);
        if ( specialCode.match('pli=0') ) {
            cssClass = 'img-responsive img-rounded';
        } else {
            cssClass = 'img-responsive pull-left img-rounded padding-to-text';
        }
        htmlContent = this.htmlService.responsifyImages(htmlContent, cssClass);
        htmlContent = this.htmlService.responsifyIframes(htmlContent);

        return htmlContent;
    }

    // -----
    // returns a record's title
    // -----
    public getTitle(record: Record) {
        let title = '';

        title = this.htmlService.shortenString(record.entry_title, 100);

        return title;
    }

    // -----
    // returns a correctly formatted link to view an entry
    //  linkType: {readMore, goToSource, internal.id.basePath}
    // -----
    public getLink(linkType: string, record: Record, mod: Module) {
        let link            = '<a href="[HREF]" target="[TARGET]">[TITLE]</a>';
        let href            = '';
        let target          = '_self';
        let title           = '';
        let bundleId        = this.getBundleId(mod);
        let externalLink    = false;

        // check to see if it is an external link
        if (
            !linkType.match('internal')                     // internal links are never external (surprise!)
            && (
                   record.feed_special_code.match('ff=0')   // if full feed is 0 it means we will not display all content on the foreNeno site
                || record.feed_special_code.match('len=')   // if there is a length limitation, no matter what the length is set to, we'll always provide an external read more link
                || linkType === 'goToSource'                // if it is a "go to source" type link
            )
        ) {
            externalLink = true;
            target = '_blank';
        }

        // ---
        // readMore link
        // ---
        if  (linkType === 'readMore') {

            // ---
            // build href
            // ---
            if (externalLink) {
                href = this.libraryService.globals.goBaseUrl + '?' + record.entry_link;

            // else (if full feed is true and there is no length limitation) we'll display the full content on the foreNeno site, hence present a link to an internal page
            } else {
                href = '/page/entry/entry_id/' + record.entry_id + bundleId;
            }

            // ---
            // build title
            // ---
            if (externalLink) {
                title = this.libraryService.globals.cfg.layout.readMoreTitle + ' ' + this.htmlService.getDomainName(record.entry_link) + ' &#8599;';
            } else {
                title = this.libraryService.globals.cfg.layout.readMoreTitle + ' ' + this.libraryService.globals.cfg.layout.siteTitle;
            }

            // check if rml (read more link) is set to 0, and we should display no link
            if (record.feed_special_code.match('rml=0')) {
                link        = '';
                href        = '';
                target      = '';
                title       = '';
                bundleId    = '';
            }

        // ---
        // goToSource link
        // ---
        } else if (linkType === 'goToSource') {
            href = this.libraryService.globals.goBaseUrl + '?' + record.entry_link;
            title = this.libraryService.globals.cfg.layout.goToSourceTitle + ' ('  + this.htmlService.getDomainName(record.entry_link) + ') '  + ' &#8599;'

            // check if gts (go to source) is set to 0, and we should display no link
            if (record.feed_special_code.match('gts=0')) {
                link        = '';
                href        = '';
                target      = '';
                title       = '';
                bundleId    = '';
            }

        // ---
        // internal link
        // ---
        } else if (linkType.match('internal')) {
            let linkTypeElements = linkType.split('.');
            let id = linkTypeElements[1];
            let basePath = linkTypeElements[2];
            href = basePath + record[id] + bundleId;
        }

        // ---
        // add a reference paramter to external links
        // ---
        if (externalLink && !record.feed_special_code.match('ref=0')) {
            href = href + '?ref=' + this.libraryService.globals.cfg.layout.siteTitle;
        }

        // build the complete link
        link = link.replace('[HREF]', href).replace('[TARGET]', target).replace('[TITLE]', title);

        // return the link and all its sub parts
        return {
              link:     link
            , href:     href
            , target:   target
            , title:    title
        };
    }

    // -----
    // returns the details of an entry
    // note: can't use css classes from elsewhere but bootstrap css from inside the templateService. hence style="" is used instead
    // -----
    public getEntryDetails(record: Record, mod: Module, url: string) {
        let htmlNoToggled   = '';
        let htmlToggeled    = '';
        let bundleId        = this.getBundleId(mod);

        // in case the bundle id is a wildcard we assume that we are given a url that contains a bundle id. hence we use the bundle id from the url
        if (bundleId === '/*' && url !== undefined) {
            bundleId = url.substring(url.lastIndexOf('/'), url.length);
        }

        // edd (entry details display)
        if (!record.feed_special_code.match('edd=0')) {
            htmlNoToggled += '<small>'+record.entry_published_date_display+'</small>';

            // gts (go to source)
            if (!record.feed_special_code.match('gts=0')){
                htmlToggeled += ' &#9137; ';
                htmlToggeled += this.getLink('goToSource', record, mod).link;
            }

            htmlToggeled += ' &#9137; ';
            htmlToggeled += '<a href="/page/profile/feed_profile_id/'+record.feed_profile_id+bundleId+'">'+record.feed_profile+'</a>';
            htmlToggeled += ' &#9137; ';
            htmlToggeled += '<a href="/page/maincat/feed_main_category_id/'+record.feed_main_category_id+bundleId+'">'+record.feed_main_category+'</a>';
            htmlToggeled += ' &#9137; ';
            htmlToggeled += '<a href="/page/subcat/feed_sub_category_id/'+record.feed_sub_category_id+bundleId+'">'+record.feed_sub_category+'</a>';
            htmlToggeled += ' &#9137; ';
            htmlToggeled += '<a href="/page/feed/feed_id/'+record.feed_id+bundleId+'">'+record.feed_source+'</a>';

            // eld (entry label display)
            if (!record.feed_special_code.match('eld=0')){
                htmlToggeled += ' &#9137; ';
                for (let tag in record.entry_tags) {
                    htmlToggeled += record.entry_tags[tag] + ' ';
                }
            }
        }

        return {
              htmlNoToggled:    htmlNoToggled
            , htmlToggeled:     htmlToggeled
        };
    }

    // -----
    // returns the entry's image to be displayed
    // -----
    public getDisplayImage(record: Record) {
        let returnHtml      = '';
        let imgSrc          = this.htmlService.getFirstImage(record.entry_content_html);
        let cssClass        = '';
        let displayImage    = false;

        // determine whether or not we shall display an image
        if (
               imgSrc
            && !record.feed_special_code.match('mvc=0')
            && (record.feed_special_code.match('bil=1') || record.feed_special_code.match('bil=2'))
        ) {
            displayImage = true;
        }

        // build html code displaying the image
        if (displayImage) {
            returnHtml = '<img class="[CLASS]" src="[SRC]">';

            if ( record.feed_special_code.match('bil=1') ) {
                cssClass = 'listview-entry-image-big1 img-responsive';
            } else if ( record.feed_special_code.match('bil=2') ) {
                cssClass = 'listview-entry-image-big2 img-responsive';
            }

            returnHtml = returnHtml.replace('[CLASS]', cssClass).replace('[SRC]', imgSrc);
        }

        return returnHtml;
    }

    // -----
    // returns the entry ingress
    // -----
    public getEntryIngress(record: Record) {
        let returnHtml          = '';
        let displayThumbnail    = true;
        let displayContent      = true;

        // check if image (thumb) should be displayed
        if (
               this.getDisplayImage(record) !== ''                                                      // do not display image in content if it has been been presented as big image via "getDisplayImage()"
            || record.feed_special_code.match('mvc=0')                                                  // do not display thumb if "move visible content" aka "mvc" is set to 0
        ) {
            displayThumbnail = false;
        }

        // check if content should be displayed
        if (
               record.feed_special_code.match('mvc=0')                                                  // do not display text if "move visible content" aka "mvc" is set to 0
        ) {
            displayContent = false;
        }

        if (displayThumbnail && this.htmlService.getFirstImage(record.entry_content_html) !== '') {
            returnHtml = returnHtml + '<img class="listview-entry-thumbnail pull-left padding-to-text" src="' + this.htmlService.getFirstImage(record.entry_content_html) + '"">';
        }

        if (displayContent) {
            let maxLength = 500;

            if ( record.feed_special_code.match('len=') ) {
                // if a len(ght) value is set in the feed's special code and this length is less than default max length, then reduce the max length
                let len = parseInt(this.getFeedSpecialCodeValue('len', record.feed_special_code));
                if (len < maxLength) {
                    maxLength = len;
                }
            }

            returnHtml = returnHtml + this.htmlService.shortenString(this.prepareTextContent(record.entry_content_text),maxLength);
        }

        return returnHtml;
    }

    // -----
    // returns the entry content
    // -----
    public getEntryContent(record: Record) {
        let returnContent       = '';

        // if a len(gth) code is set we'll return a shortened version of the plain text code instead of the html
        if ( record.feed_special_code.match('len=') ) {
            returnContent = this.htmlService.shortenString(this.prepareTextContent(record.entry_content_text), parseInt(this.getFeedSpecialCodeValue('len', record.feed_special_code)));
        } else {
            returnContent = this.prepareHtmlContent(record.entry_content_html, record.feed_special_code);
        }

        return returnContent;
    }

    // -----
    // get entry navigation button link to previous or next entry
    // -----
    public getEntryNavigationLink(navigationType: string, record: Record, mod: Module, url: string) {
        let returnContent = '';
        let bundleId        = this.getBundleId(mod);

        // in case the bundle id is a wildcard we assume that we are given a url that contains a bundle id. hence we use the bundle id from the url
        if (bundleId === '/*' && url !== undefined) {
            bundleId = url.substring(url.lastIndexOf('/')+1, url.length);
        }

        // update current entry id so the we'll generate a correct link below
        if (navigationType === 'previous') {
            record.entry_id = record.entry_id_previous;
            navigationType = ' &laquo; ' + navigationType;
        } else if (navigationType === 'next') {
            record.entry_id = record.entry_id_next;
            navigationType = navigationType + ' &raquo;';
        } else {
            this.libraryService.errorMessage('unknown type: ' + navigationType);
        }

        let readMorelink = this.getLink('readMore', record, mod);

        if (record.entry_id !== undefined) {
            // update the wildcard in the rpc with the proper bundle id
            if (bundleId !== undefined) {
                readMorelink.href = readMorelink.href.replace('*', bundleId);
            }

            returnContent += '<a class="btn btn-primary" href="' + readMorelink.href + '">' + navigationType + '</a>';
        }

        return returnContent;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // PRIVATE HELP FUNCTIONS
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // -----
    // given a string of names with values extract the value of one given name
    // -----
    private getFeedSpecialCodeValue(variable: string, feed_special_code: string) {
        // example:
        // variable             = 'len'
        // feed_special_code    = 'ff=1 len=99 gts=0'
        feed_special_code = feed_special_code.split(variable+'=')[1];
        // new data after above code has been executed:
        // feed_special_code    = '99 gts=0'

        // matching the first digits, in $1
        let re  = /^(\d+)/;

        // none standard js reg exp usage. however, supported by all important browsers
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/n
        re.test(feed_special_code);
        let returnValue = RegExp.$1;

        return returnValue;
    }

    // -----
    // returns a bundle id from a module
    // -----
    private getBundleId(mod: Module){
        let bundleId = '';

        if ( mod !== undefined ) {
            bundleId = this.libraryService.getBundleId(mod);
            if (bundleId) {
                bundleId = '/' + bundleId;
            } else {
                bundleId = '';
            }
        }

        return bundleId;
    }

}
