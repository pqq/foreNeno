import { Injectable }       from '@angular/core';
import { Router }           from '@angular/router';

import { BehaviorSubject }  from 'rxjs/BehaviorSubject';
import { Subject }          from 'rxjs/Subject';

import { CfgService }       from '../services/cfg.service.KDBUILD';
import { HtmlService }      from '../services/html.service.KDBUILD';
import { Cfg }              from '../classes/cfg.class.KDBUILD';
import { ServiceList }      from '../classes/servicelist.class.KDBUILD';
import { Module }           from '../classes/module.class.KDBUILD';
import { Spawn }            from '../classes/spawn.class.KDBUILD';
import { SPAWN }            from '../mocks/spawn.mock.KDBUILD';
import { Record }           from '../classes/record.class.KDBUILD';

/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    description:
        - foreNeno specific service/functions
        - other help functions that does not fit in other services
        - stores data, global data/values, that is shared by the all parts of the foreNeno app

    foreNeno dependencies:
        - CfgService
        - HtmlService

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
@Injectable()
export class LibraryService {

    // -----
    // global values
    // -----
    public globals: {
          cfgSubject:           Subject<Cfg>
        , cfg:                  Cfg
        , entryDetailsToggle:   string
        , loadingStatus:        string
        , navbarClasses:        string
        , rpcBaseUrl:           string
        , goBaseUrl:            string
    } = {
          cfgSubject:           new BehaviorSubject<Cfg>(null)
        , cfg:                  undefined
        , entryDetailsToggle:   ''
        , loadingStatus:        ''
        , navbarClasses:        ''
        , rpcBaseUrl:           ''
        , goBaseUrl:            ''
    };

    // -----
    // constructor
    // -----
    constructor(
          private router: Router
        , private htmlService: HtmlService
        , private cfgService: CfgService
    ) {
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // PUBLIC HELP FUNCTIONS
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // -----
    // print error message to console
    // -----
    public errorMessage(message: string) {
        console.log('ERROR: ' + message);
    }

    // -----
    // print to console, for debugging purposes
    // -----
    public debug(anything: any) {
        console.log(anything);
    }

    // -----
    // check the existence of something, write error message if undefined
    // -----
    public checkExistence(message: string, something: any) {
        if (!something) {
            this.errorMessage(message);
        }
    }

    // -----
    // replace last occurrence of a string in a string
    // -----
    public replaceLast(fullString: string, whatToReplace: string, replacement: string) {
        let pcs = fullString.split(whatToReplace);
        let lastPc = pcs.pop();
        return pcs.join(whatToReplace) + replacement + lastPc;
    };

    // -----
    // toggle mobile menu on/off
    // -----
    public toggleMobileMenu() {
        this.checkExistence('library.service.toggleMobileMenu(): no config object registered!', this.globals.cfg)
        if (this.globals.cfg.layout.mobileMenuView === 'show') {
            this.globals.cfg.layout.mobileMenuView = 'hide';
        } else {
            this.globals.cfg.layout.mobileMenuView = 'show';
        }
    }

    // -----
    // toggle open/close
    // -----
    public toggleOpenClose() {
        if ( this.globals.entryDetailsToggle === "+" || this.globals.entryDetailsToggle === "") {
            this.globals.entryDetailsToggle = "x";
        } else {
            this.globals.entryDetailsToggle = "+";
        }
    }

    // -----
    // goto function
    //   where = {about, welcome, module}
    //   corresponding path has to be defined in `app-routing.module`
    // -----
    public goto(where: string, path: string = undefined) {
        let link: string[];

        if (where === 'welcome') {
            link = ['welcome'];
        } else if (where === 'about') {
            link = ['about'];
        } else if (where === 'module') {
            if (path === undefined) {
                this.errorMessage('library.service.goto(): missing path!');
            } else {
                link = [path];
            }
        }

        if (link === undefined) {
            this.errorMessage('library.service.goto(): no link found for "' +where+ '"');
        } else {
            this.router.navigate(link);
            this.htmlService.scrollToTop()
        }
    }

    // -----
    // check to see if a url contains a bundle_id or not
    // -----
    public isBundleUrl (url: string) {
        let bundleUrl = false;
        let numberOfSlashes = (url.match(/\//g) || []).length;

        // if there are 4 slashes the url contains a bundle_id, like: "page/maincat/:id_name/:id_value/blog"
        // if there are 3 slashes the url is without a bundle_id, like: "page/maincat/:id_name/:id_value"
        if (numberOfSlashes === 4) {
            bundleUrl = true;
        }

        return bundleUrl;
    }

    // -----
    // check to see if an module exists or not
    // -----
    public isModule (url: string, cfg: Cfg) {
        let moduleFound = false;
        let modules: Module[];
        modules = cfg.modules;
        url = url.toString().replace(/,/g, '/');

        for (let idx in modules) {
            if (modules[idx].path === url) {
                moduleFound = true;
                break;
            }
        }

        return moduleFound;
    }

    // -----
    // returns a module for a service, given a url
    // -----
    public getModule (url: string, cfg: Cfg) {
        let modules: Module[];
        let mod: Module;

        // the reason for the replace:
        //   urls from angular is using comma (eg. "entrylist,news"), while
        //   routePath is using slash (eg. "entrylist/news")
        url = url.toString().replace(/,/g, '/');

        // if it is a url with a bundle_id then we replace the bundle id with wildcard '*'
        let isBundleUrl = this.isBundleUrl(url);
        if (isBundleUrl) {
            let isModule = this.isModule(url, cfg);
            if (!isModule) {
                url = url.substring(0, url.lastIndexOf('/')) + '/*';
            }
        }

        modules = cfg.modules;

        // loop through all modules
        for (let idx in modules) {
            if (modules[idx].path === url) {
                mod = modules[idx];
                break;
            }
        }

        if (mod === undefined) {
            this.errorMessage('library.service.getModule(): no module found for url "' +url+ '"');
        }

        return mod;
    }

    // -----
    // returns the service id for the running service
    // -----
    public getServiceId() {
        let serviceList     = new ServiceList();
        let hostname        = this.htmlService.getHostName();
        let serviceId     = '';

        for (let idx in serviceList.services) {
            if (hostname.indexOf(serviceList.services[idx].domain) > -1) {
                serviceId = serviceList.services[idx].serviceId
            }
        }

        if (!serviceId) {
            this.errorMessage('library.service.getServiceName(): no service name given for current domain "' + hostname + '"');
        }

        return serviceId;
    }

    // -----
    // returns a bundle id for a service, given a module
    // -----
    public getBundleId (mod: Module) {
        let bundleId: string;
        let rpcParams: string;
        let idx: number;

        if (mod === undefined) {
            this.errorMessage('library.service.getBundleId(): no module, no bundle!');
        }

        if (mod.rpcParams === undefined) {
            this.errorMessage('library.service.getBundleId(): no rpcParams!');
        }

        rpcParams = mod.rpcParams;
        idx = rpcParams.search('bundle_id=');

        if (idx >= 0) {
            // remove all before 'bundle='
            rpcParams = rpcParams.slice(idx, rpcParams.length);
            // remove all after the first '&'
            idx = rpcParams.search('&');
            if (idx >= 0) {
                rpcParams = rpcParams.slice(0, idx);
            }
            // remove all before '='
            idx = rpcParams.search('=');
            rpcParams = rpcParams.slice(idx+1, rpcParams.length);

            bundleId = rpcParams;
        }

        return bundleId;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // PRIVATE HELP FUNCTIONS
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // -----
    // returns the environment name
    // -----
    private getEnvironmentId() {
        let hostname = this.htmlService.getHostName();
        let environmentId = '';

        if (hostname === '' || hostname === 'localhost') {
            environmentId = 'dev';
        } else if (hostname.match('beta')) {
            environmentId = 'beta';
        } else {
            environmentId = 'prod';
        }

        return environmentId;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // PUBLIC MAIN FUNCTIONS
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // -----
    // spawn, where it all starts, this initiates the app by loading crucial data, and triggering life
    // -----
    public spawn() {
        let environment = this.getEnvironmentId();

        Promise.resolve(SPAWN).then(
            (spawn: Spawn) => {

                if (environment === 'dev') {
                    this.globals.rpcBaseUrl = spawn.rpcBaseUrl.dev;
                    this.globals.goBaseUrl = spawn.goBaseUrl.dev;
                } else if (environment === 'beta') {
                    this.globals.rpcBaseUrl = spawn.rpcBaseUrl.beta;
                    this.globals.goBaseUrl = spawn.goBaseUrl.beta;
                } else if (environment === 'prod') {
                    this.globals.rpcBaseUrl = spawn.rpcBaseUrl.prod;
                    this.globals.goBaseUrl = spawn.goBaseUrl.prod;
                }

                if (!this.globals.rpcBaseUrl) {
                    this.errorMessage('library.service.spawn(): for you i am truly sorry. the first step towards life can not be taken.');
                }

                if (!this.globals.goBaseUrl) {
                    this.errorMessage('library.service.spawn(): i sense problems in the horison.');
                }

                this.live();
            }
        );
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // PRIVATE MAIN FUNCTIONS
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // -----
    // after spawn there is life. live() loads configuration and runs initial tasks.
    // -----
    private live() {
        let serviceId = this.getServiceId();
        let environmentId = this.getEnvironmentId();
        let url = this.globals.rpcBaseUrl + '?service_id=' + serviceId + '&env_id=' + environmentId + '&bundle_id=core.cfg&method=list_entries_json';

        // add a pointer from the configuration subject in library service, to cfg.service. this so that we
        // elsewhere in the application do not need to inject cfg.service
        this.globals.cfgSubject = this.cfgService.cfgSubject;

        // initiate the configuration service
        this.cfgService.initiate(url, serviceId, environmentId);

        // store configuration and set title when configuration is done loading
        this.cfgService.cfgSubject.subscribe(
            (cfg: Cfg) => {
                if (cfg) {
                    this.globals.cfg = cfg;

                    // set meta tags as we go live. common tags for all pages within a service.
                    this.htmlService.setMetaTags([
                          { name: 'author',   content: cfg.meta.author}
                        , { name: 'keywords',   content: cfg.meta.keywords}
                        , { name: 'description',   content: cfg.meta.description}
                    ]);
                }
            }
        );
    }

}
