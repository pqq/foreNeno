import { Injectable }       from '@angular/core';

import { Observable }       from 'rxjs/Observable';
import 'rxjs/add/observable/throw';

import { HttpService }      from '../services/http.service.KDBUILD';
import { LibraryService }   from '../services/library.service.KDBUILD';
import { Cfg }              from '../classes/cfg.class.KDBUILD';

/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    description:
        - service responsible for all rpcs (remote procedure calls)

    foreNeno dependencies:
        - LibraryService

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
@Injectable()
export class RpcService {

    constructor(
          private httpService: HttpService
        , private libraryService: LibraryService
    ) {
    }

    // -----
    // build a complete service url, based on a string of parameters
    // -----
    completeServiceUrl(parameterstring: string, cfg: Cfg) {
        let finalisedServiceUrl = this.libraryService.globals.rpcBaseUrl + '?service_id=' + cfg.rpc.serviceId + '&env_id=' + cfg.rpc.envId + '&cache=' + cfg.rpc.cache + '&' + parameterstring;
        return finalisedServiceUrl;
    }

    // -----
    // rpc call, given a url and a pageType (modules.pages.type) to check
    // idName : the name of the id, as defined in 'app-routing.ts', example, in the router path 'page/:page_type/:identifier' 'identifier' is the name of the id
    // idValue : the id itself. 'identifier' can for example have the value '3a9c6e7f8c3b68ed' for single entry views
    // -----
    rpcByUrl(url: string, cfg: Cfg, idName: string = undefined, idValue: string = undefined) {
        let rpc: Observable<any>;
        let rpcParams: string;
        let mod: any;
        let urlTmp: string;
        let bundleId: string;

        // url from angular contains comma, like "entrylist,news", while routePath is using slash, like "entrylist/news". hence replace.
        url = url.replace(/,/g, '/');
        urlTmp = url;

        // turn on loading gif
        this.libraryService.globals.loadingStatus = 'loading';

        // if it is a url with a bundle_id then we replace the bundle id with wildcard '*'
        let isBundleUrl = this.libraryService.isBundleUrl(url);
        if (isBundleUrl) {
            let isModule = this.libraryService.isModule(url, cfg);
            if (!isModule) {
                bundleId = url.substring(url.lastIndexOf('/')+1, url.length);
                url = url.substring(0, url.lastIndexOf('/')) + '/*';
            }
        }

        // get rpc parameters
        mod = this.libraryService.getModule(url, cfg);
        if (mod !== undefined) {
            rpcParams = mod.rpcParams;
        }

        // we might have manipulated the url so it contains a wildcard instead of bundle_id. hence, update it back to before the manipulation.
        url = urlTmp;

        // update the wildcard in the rpc with the proper bundle id
        if (rpcParams !== undefined && bundleId !== undefined) {
            rpcParams = rpcParams.replace('*', bundleId);
        }

        // if idName and idValue is given, attach those to the rpc
        if (idName !== undefined && idValue !== undefined && rpcParams !== undefined) {
            rpcParams = rpcParams  + '&' + idName + '=' + idValue;
        }

        if (rpcParams === undefined) {
            this.libraryService.errorMessage('rpc.service.rpcByUrl(): unable to locate rpc parameters for the url "' + url + '"');
            this.libraryService.globals.loadingStatus = 'error';
            rpc = Observable.throw('we are ending things here.');
        } else {
            rpc = this.rpc(rpcParams, cfg);
        }

        return rpc;
    }

    // -----
    // rpc call, given a set of parameters
    // -----
    rpc(parameterstring: string, cfg: Cfg) {
        let serviceUrl = this.completeServiceUrl(parameterstring, cfg);
        return this.httpService.get(serviceUrl).map(
            (response: any) => (response.result_set)
        );
    }

    // -----
    // multiple, forked, rpc calls, given an array of parameter strings
    // -----
    rpcForkJoin(parameterstrings: string[]) {
        // <!> if we start using rpcForkJoin, the array of parameter strings needs to be converted to urls <!>
        let urls = parameterstrings;
        return this.httpService.forkJoin(urls);
    }

}
