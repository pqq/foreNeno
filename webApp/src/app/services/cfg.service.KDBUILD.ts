import { Injectable }           from '@angular/core';

import { BehaviorSubject }      from 'rxjs/BehaviorSubject';
import { Subject }              from 'rxjs/Subject';

import { HttpService }          from '../services/http.service.KDBUILD';
import { Cfg }                  from '../classes/cfg.class.KDBUILD';
import { RpcResponse }          from '../classes/rpcresponse.class.KDBUILD';
import { CfgResponse }          from '../classes/cfgresponse.class.KDBUILD';
import { ServiceConfig }        from '../classes/serviceconfig.class.KDBUILD';

/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    description:
        - loads foreNeno configuration data
        - using Rx's BehaviorSubject for handeling the configuration data, for avoiding loading data more than once during bootstrapping the application

    foreNeno dependencies:
        - HttpService

    documentation:
        - https://xgrommx.github.io/rx-book/content/subjects/behavior_subject/index.html
        - https://github.com/klevstul/foreneNo/issues/313#issuecomment-298417390

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
@Injectable()
export class CfgService {
    constructor(
        private httpService: HttpService
    ) {}

    // -----
    // configuration as a subject, so that we can subscribe to it for ensuring only using it when it has been loaded
    // -----
    public cfgSubject: Subject<Cfg> = new BehaviorSubject<Cfg>(null);

    // -----
    // function to set/broadcast a new configuration
    // -----
    private setConfiguration(cfg: Cfg) {
            this.cfgSubject.next(cfg);
    }

    // -----
    // load configuration data from a given url
    // -----
    private load(url: string) {
        return this.httpService.get(url).map(
            (rpcResponse: RpcResponse) => (this.extractData(rpcResponse))
        );
    }

    // -----
    // extract data
    // -----
    private extractData(res: RpcResponse) {
        return res.result_set[0] || { };
    }

    // -----
    // merge two libs, where settings in libTwo will overwrite libOne
    //   ref: https://stackoverflow.com/questions/171251/how-can-i-merge-properties-of-two-javascript-objects-dynamically
    // -----
    private mergeLibs(libOne: any, libTwo: any) {
        for (let p in libTwo) {
            try {
                // Property in destination object set; update its value.
                if ( libTwo[p].constructor==Object ) {
                    libOne[p] = this.mergeLibs(libOne[p], libTwo[p]);
                } else {
                    libOne[p] = libTwo[p];
                }
            } catch(e) {
                // Property in destination object not set; create it and set its value.
                libOne[p] = libTwo[p];
            }
        }

        return libOne;
    }

    // -----
    // initiate configuration, loads configuration from a url, sets the subject and returns the cfg
    // -----
    public initiate(url: string, serviceId: string, environmentId: string) {

        let defaultCfg: Cfg;
        let serviceConfig: ServiceConfig;
        let environmentSpecificCfg: Cfg;
        let mergedConfiguration: Cfg;

        this.load(url)
        .subscribe(
            (cfgResponse: CfgResponse) => {

                // print error in case there is no response
                if (!cfgResponse) {
                    // to avoid reduntant dependencies we do not use the error function from the library service
                    console.log('ERROR: cfg.service.initiate(): no life in the universe was found');
                }

                // for being able to use functions inside the class CfgResponse we need to initiate a new object from the class
                let newCfgResponse = new CfgResponse(cfgResponse);

                // get the service configuration from the response object
                serviceConfig = newCfgResponse.get(serviceId);

                // check existence
                if (!serviceConfig) {
                    console.log('ERROR: cfg.service.initiate(): i can not even see the planet i am supposed to live on');
                }

                // get default configuration
                defaultCfg = serviceConfig.default;

                // get the environment specific configuration
                if (environmentId === 'dev') {
                    environmentSpecificCfg =  serviceConfig.dev;
                } else if (environmentId === 'beta') {
                    environmentSpecificCfg =  serviceConfig.beta;
                } else if (environmentId === 'prod') {
                    environmentSpecificCfg =  serviceConfig.prod;
                } else {
                    console.log('ERROR: cfg.service.initiate(): it might sound strange but i have no environment to grow up in');
                }

                // a default configuration object is required
                if (!defaultCfg) {
                    console.log('ERROR: cfg.service.initiate(): no guidelines found hence no life can be granted');
                }

                // merge two libs, as long as the env specific is given as second argument it can be empty
                mergedConfiguration = this.mergeLibs(defaultCfg, environmentSpecificCfg);

                // set the loaded configuration object
                this.setConfiguration(mergedConfiguration);

            }
            , err => {
                console.error('ERROR: cfg.service.initiate(): ' + err);
            }
            , () => {
            }
        );

    }

}
