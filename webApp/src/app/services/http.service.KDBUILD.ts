import { Injectable }       from '@angular/core';
import { Http, Response }   from '@angular/http';
import { Observable }       from 'rxjs/Observable';
import                      'rxjs/add/operator/map';
import                      'rxjs/add/operator/catch';
import                      'rxjs/add/observable/forkJoin';

/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    description:
        - generic (none foreNeno specific) http request functions

    foreNeno dependencies:
        - NONE (this service must be completely independent from all other parts of the app, so it can be used everywhere.)

    documentation:
        - https://angular.io/docs/ts/latest/guide/server-communication.html

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
@Injectable()
export class HttpService {

    constructor(
        private http: Http
    ) {}

    // -----
    // get data from given url and return an observable
    // -----
    get(url: string) {
        return this.http.get(url)
                                .map(
                                    (response: Response) =>  (this.extractData(response))
                                )
                                .catch(
                                    (err:Response) => {
                                        this.handleError(err);
                                        return Observable.throw(err);
                                    }
                                );
    }

    // -----
    // multiple, forked, http requests, given an array of urls
    // -----
    forkJoin(urls: string[]) {
        let observables = [];
        for (let idx in urls) {
            observables.push(
                this.http
                    .get(urls[idx])
                    .map(
                        response => this.extractData(response)
                    ).catch(
                        (err:Response) => {
                            this.handleError(err);
                            return Observable.throw(err);
                        }
                    )
            );
        }
        return Observable.forkJoin(observables);
    }

    // -----
    // extract data from a response
    // -----
    private extractData(res: Response) {
        let resultAsJson = res.json();

        // in case the result json contains an error field we output the info
        if (resultAsJson.error){
            console.error(resultAsJson);
        }

        return resultAsJson || { };
    }

    // -----
    // handle errors
    // -----
    private handleError (error: Response | any) {
        let errMsg: string;

        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
    }

}
