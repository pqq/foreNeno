import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AboutComponent }       from '../components/about.component.KDBUILD';
import { IframeComponent }      from '../components/iframe.component.KDBUILD';
import { InstagramComponent }   from '../components/instagram.component.KDBUILD';
import { ListComponent }        from '../components/list.component.KDBUILD';
import { PageComponent }        from '../components/page.component.KDBUILD';
import { StaticComponent }      from '../components/static.component.KDBUILD';
import { WelcomeComponent }     from '../components/welcome.component.KDBUILD';

const routes: Routes = [
      { path: '', redirectTo: 'welcome', pathMatch: 'prefix' }
    , { path: 'about', component: AboutComponent }
    , { path: 'iframe/:type', component: IframeComponent }
    , { path: 'instagram/:type', component: InstagramComponent }
    , { path: 'list/:list_type/:list_name', component: ListComponent }
    , { path: 'page/:page_type/:id_name/:id_value', component: PageComponent }
    , { path: 'page/:page_type/:id_name/:id_value/:bundle_id', component: PageComponent }
    , { path: 'static/:page_name', component: StaticComponent }
    , { path: 'welcome', component: WelcomeComponent }
    , { path: '**', redirectTo: 'welcome' }
];

@NgModule({
      imports: [ RouterModule.forRoot(routes) ]
    , exports: [ RouterModule ]
})
export class AppRoutingModule {}
