import { NgModule, ErrorHandler }   from '@angular/core';
import { BrowserModule, Title }     from '@angular/platform-browser';
import { HttpModule }               from '@angular/http';

import Raven = require('raven-js');

import { AppRoutingModule }         from './app-routing.module.KDBUILD';

import { AppComponent }             from '../components/app.component.KDBUILD';

import { RpcService }               from '../services/rpc.service.KDBUILD';
import { LibraryService }           from '../services/library.service.KDBUILD';
import { HttpService }              from '../services/http.service.KDBUILD';
import { HtmlService }              from '../services/html.service.KDBUILD';
import { CfgService }               from '../services/cfg.service.KDBUILD';
import { TemplateService }          from '../services/template.service.KDBUILD';

import { AboutComponent }           from '../components/about.component.KDBUILD';
import { IframeComponent }          from '../components/iframe.component.KDBUILD';
import { InstagramComponent }       from '../components/instagram.component.KDBUILD';
import { ListComponent }            from '../components/list.component.KDBUILD';
import { PageComponent }            from '../components/page.component.KDBUILD';
import { StaticComponent }          from '../components/static.component.KDBUILD';
import { WelcomeComponent }         from '../components/welcome.component.KDBUILD';

import { SafeUrlPipe }              from '../pipes/safeurl.pipe.KDBUILD';
import { SafeHtmlPipe }             from '../pipes/safehtml.pipe.KDBUILD';

Raven.config('https://178a7a8b716f4b82a035d4c1a854d121@sentry.io/178386', {
    shouldSendCallback: function () {
        if (window.location.hostname !== 'localhost'){
            return true;
        } else {
            return false;
        }
    }
}).install();

export class RavenErrorHandler implements ErrorHandler {
    handleError(err:any) : void {
        Raven.captureException(err.originalError || err);

        if (window.location.hostname === 'localhost'){
            console.log(err.originalError || err);
        }
    }
}

@NgModule({
  imports:      [
        BrowserModule
      , HttpModule
      , AppRoutingModule
  ],
  declarations: [
        AppComponent
      , AboutComponent
      , IframeComponent
      , InstagramComponent
      , ListComponent
      , PageComponent
      , StaticComponent
      , WelcomeComponent
      , SafeUrlPipe
      , SafeHtmlPipe
  ],
  providers:    [
        Title
      , RpcService
      , LibraryService
      , HttpService
      , HtmlService
      , CfgService
      , TemplateService
      , { provide: ErrorHandler, useClass: RavenErrorHandler }
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule {

    // it is said that if you register a service at a global scope, here in app.module, you can
    // inject it anywhere throughout the app without having to injecting again as a dependecy via
    // the constructor. i can not see that this is working. even in the official angular tutorial
    // (https://angular.io/docs/ts/latest/tutorial/toh-pt4.html) the "HeroService" is re-injected
    // through out the app, despite it has been declared/injected at the top level, inside
    // app.module. so, since this is not working as expected (i must be misunderstanding something)
    // i instead spawn() in app.component, which is a more logical place as well.

    constructor() {}

 }
