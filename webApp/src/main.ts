import { platformBrowserDynamic }   from '@angular/platform-browser-dynamic';
import { enableProdMode }           from '@angular/core';

import { AppModule }                from './app/modules/app.module.KDBUILD';


if (window.location.hostname !== 'localhost'){
    enableProdMode();
}
platformBrowserDynamic().bootstrapModule(AppModule);
