ECHO ON
CLS

:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
:: what:        copy required files from "node_modules" folder used for development, into webapp directory used in production
:: author:      frode klevstul (frode at klevstul dot com)
:: started:     march 2017
:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -




:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
:: PROJECT SPECIFIC CONFIGURATION - TO BE CONFIGURED FOR EACH PROJECT
:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

:: - - - - -
:: set project specific directory (full path)
:: - - - - -
SET NODEMODULESDIR=R:\googleDrive\projects\_gitHub\foreNeno\webApp\node_modules
SET TARGETDIRMISC=R:\googleDrive\projects\_gitHub\foreNeno\webApp\src\npm\misc
SET TARGETDIRRXJS=R:\googleDrive\projects\_gitHub\foreNeno\webApp\src\npm\rxjs

:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
:: MAKE DIRECTORIES
:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
if not exist "%TARGETDIRMISC%\" mkdir %TARGETDIRMISC%\
if not exist "%TARGETDIRRXJS%\observable\" mkdir %TARGETDIRRXJS%\observable\
if not exist "%TARGETDIRRXJS%\operator\" mkdir %TARGETDIRRXJS%\operator\
if not exist "%TARGETDIRRXJS%\util\" mkdir %TARGETDIRRXJS%\util\
if not exist "%TARGETDIRRXJS%\symbol\" mkdir %TARGETDIRRXJS%\symbol\
if not exist "%TARGETDIRRXJS%\add\observable\" mkdir %TARGETDIRRXJS%\add\observable\
if not exist "%TARGETDIRRXJS%\add\operator\" mkdir %TARGETDIRRXJS%\add\operator\

:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
:: COPY
:: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

:: raven-js
copy %NODEMODULESDIR%\raven-js\dist\raven.js %TARGETDIRMISC%\.

:: core-js, zone & systemjs
copy %NODEMODULESDIR%\core-js\client\shim.min.js %TARGETDIRMISC%\.
copy %NODEMODULESDIR%\core-js\client\shim.min.js.map %TARGETDIRMISC%\.
copy %NODEMODULESDIR%\zone.js\dist\zone.js %TARGETDIRMISC%\.
copy %NODEMODULESDIR%\systemjs\dist\system.src.js %TARGETDIRMISC%\.
copy %NODEMODULESDIR%\systemjs\dist\system.src.js.map %TARGETDIRMISC%\.

:: angular
copy %NODEMODULESDIR%\@angular\platform-browser-dynamic\bundles\platform-browser-dynamic.umd.js %TARGETDIRMISC%\.
copy %NODEMODULESDIR%\@angular\platform-browser-dynamic\bundles\platform-browser-dynamic.umd.js.map %TARGETDIRMISC%\.
copy %NODEMODULESDIR%\@angular\core\bundles\core.umd.js %TARGETDIRMISC%\.
copy %NODEMODULESDIR%\@angular\core\bundles\core.umd.js.map %TARGETDIRMISC%\.
copy %NODEMODULESDIR%\@angular\compiler\bundles\compiler.umd.js %TARGETDIRMISC%\.
copy %NODEMODULESDIR%\@angular\compiler\bundles\compiler.umd.js.map %TARGETDIRMISC%\.
copy %NODEMODULESDIR%\@angular\common\bundles\common.umd.js %TARGETDIRMISC%\.
copy %NODEMODULESDIR%\@angular\common\bundles\common.umd.js.map %TARGETDIRMISC%\.
copy %NODEMODULESDIR%\@angular\platform-browser\bundles\platform-browser.umd.js %TARGETDIRMISC%\.
copy %NODEMODULESDIR%\@angular\platform-browser\bundles\platform-browser.umd.js.map %TARGETDIRMISC%\.
copy %NODEMODULESDIR%\@angular\http\bundles\http.umd.js %TARGETDIRMISC%\.
copy %NODEMODULESDIR%\@angular\http\bundles\http.umd.js.map %TARGETDIRMISC%\.
copy %NODEMODULESDIR%\@angular\router\bundles\router.umd.js %TARGETDIRMISC%\.
copy %NODEMODULESDIR%\@angular\router\bundles\router.umd.js.map %TARGETDIRMISC%\.

:: rxjs - /
copy %NODEMODULESDIR%\rxjs\Observable.js %TARGETDIRRXJS%\.
copy %NODEMODULESDIR%\rxjs\Observable.js.map %TARGETDIRRXJS%\.
copy %NODEMODULESDIR%\rxjs\Subject.js %TARGETDIRRXJS%\.
copy %NODEMODULESDIR%\rxjs\Subject.js.map %TARGETDIRRXJS%\.
copy %NODEMODULESDIR%\rxjs\Subscriber.js %TARGETDIRRXJS%\.
copy %NODEMODULESDIR%\rxjs\Subscriber.js.map %TARGETDIRRXJS%\.
copy %NODEMODULESDIR%\rxjs\SubjectSubscription.js %TARGETDIRRXJS%\.
copy %NODEMODULESDIR%\rxjs\SubjectSubscription.js.map %TARGETDIRRXJS%\.
copy %NODEMODULESDIR%\rxjs\Subscription.js %TARGETDIRRXJS%\.
copy %NODEMODULESDIR%\rxjs\Subscription.js.map %TARGETDIRRXJS%\.
copy %NODEMODULESDIR%\rxjs\Observer.js %TARGETDIRRXJS%\.
copy %NODEMODULESDIR%\rxjs\Observer.js.map %TARGETDIRRXJS%\.
copy %NODEMODULESDIR%\rxjs\OuterSubscriber.js %TARGETDIRRXJS%\.
copy %NODEMODULESDIR%\rxjs\OuterSubscriber.js.map %TARGETDIRRXJS%\.
copy %NODEMODULESDIR%\rxjs\InnerSubscriber.js %TARGETDIRRXJS%\.
copy %NODEMODULESDIR%\rxjs\InnerSubscriber.js.map %TARGETDIRRXJS%\.
copy %NODEMODULESDIR%\rxjs\BehaviorSubject.js %TARGETDIRRXJS%\.
copy %NODEMODULESDIR%\rxjs\BehaviorSubject.js.map %TARGETDIRRXJS%\.
copy %NODEMODULESDIR%\rxjs\Notification.js %TARGETDIRRXJS%\.
copy %NODEMODULESDIR%\rxjs\Notification.js.map %TARGETDIRRXJS%\.

:: rxjs - /symbol
copy %NODEMODULESDIR%\rxjs\symbol\observable.js %TARGETDIRRXJS%\symbol\.
copy %NODEMODULESDIR%\rxjs\symbol\observable.js.map %TARGETDIRRXJS%\symbol\.
copy %NODEMODULESDIR%\rxjs\symbol\rxSubscriber.js %TARGETDIRRXJS%\symbol\.
copy %NODEMODULESDIR%\rxjs\symbol\rxSubscriber.js.map %TARGETDIRRXJS%\symbol\.
copy %NODEMODULESDIR%\rxjs\symbol\iterator.js %TARGETDIRRXJS%\symbol\.
copy %NODEMODULESDIR%\rxjs\symbol\iterator.js.map %TARGETDIRRXJS%\symbol\.

:: rxjs - /util
copy %NODEMODULESDIR%\rxjs\util\root.js %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\root.js.map %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\toSubscriber.js %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\toSubscriber.js.map %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\ObjectUnsubscribedError.js %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\ObjectUnsubscribedError.js.map %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\isScheduler.js %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\isScheduler.js.map %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\isFunction.js %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\isFunction.js.map %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\isArray.js %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\isArray.js.map %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\isObject.js %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\isObject.js.map %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\tryCatch.js %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\tryCatch.js.map %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\errorObject.js %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\errorObject.js.map %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\UnsubscriptionError.js %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\UnsubscriptionError.js.map %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\subscribeToResult.js %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\subscribeToResult.js.map %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\isArrayLike.js %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\isArrayLike.js.map %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\isPromise.js %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\isPromise.js.map %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\EmptyError.js %TARGETDIRRXJS%\util\.
copy %NODEMODULESDIR%\rxjs\util\EmptyError.js.map %TARGETDIRRXJS%\util\.

:: rxjs - /observable
if not exist "%TARGETDIRRXJS%\observable\" mkdir %TARGETDIRRXJS%\observable\
copy %NODEMODULESDIR%\rxjs\observable\merge.js %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\merge.js.map %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\ArrayObservable.js %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\ArrayObservable.js.map %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\ConnectableObservable.js %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\ConnectableObservable.js.map %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\ScalarObservable.js %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\ScalarObservable.js.map %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\EmptyObservable.js %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\EmptyObservable.js.map %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\from.js %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\from.js.map %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\of.js %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\of.js.map %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\fromPromise.js %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\fromPromise.js.map %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\forkJoin.js %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\forkJoin.js.map %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\throw.js %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\throw.js.map %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\FromObservable.js %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\FromObservable.js.map %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\PromiseObservable.js %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\PromiseObservable.js.map %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\ForkJoinObservable.js %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\ForkJoinObservable.js.map %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\ErrorObservable.js %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\ErrorObservable.js.map %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\IteratorObservable.js %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\IteratorObservable.js.map %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\ArrayLikeObservable.js %TARGETDIRRXJS%\observable\.
copy %NODEMODULESDIR%\rxjs\observable\ArrayLikeObservable.js.map %TARGETDIRRXJS%\observable\.

:: rxjs - /operator
copy %NODEMODULESDIR%\rxjs\operator\share.js %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\share.js.map %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\merge.js %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\merge.js.map %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\multicast.js %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\multicast.js.map %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\mergeAll.js %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\mergeAll.js.map %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\concatMap.js %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\concatMap.js.map %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\every.js %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\every.js.map %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\first.js %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\first.js.map %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\last.js %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\last.js.map %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\map.js %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\map.js.map %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\mergeMap.js %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\mergeMap.js.map %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\reduce.js %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\reduce.js.map %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\catch.js %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\catch.js.map %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\concatAll.js %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\concatAll.js.map %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\filter.js %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\filter.js.map %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\switchMap.js %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\switchMap.js.map %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\observeOn.js %TARGETDIRRXJS%\operator\.
copy %NODEMODULESDIR%\rxjs\operator\observeOn.js.map %TARGETDIRRXJS%\operator\.

:: rxjs - /add/observable
copy %NODEMODULESDIR%\rxjs\add\observable\throw.js %TARGETDIRRXJS%\add\observable\.
copy %NODEMODULESDIR%\rxjs\add\observable\throw.js.map %TARGETDIRRXJS%\add\observable\.
copy %NODEMODULESDIR%\rxjs\add\observable\forkJoin.js %TARGETDIRRXJS%\add\observable\.
copy %NODEMODULESDIR%\rxjs\add\observable\forkJoin.js.map %TARGETDIRRXJS%\add\observable\.

:: rxjs - /add/operator
copy %NODEMODULESDIR%\rxjs\add\operator\map.js %TARGETDIRRXJS%\add\operator\.
copy %NODEMODULESDIR%\rxjs\add\operator\map.js.map %TARGETDIRRXJS%\add\operator\.
copy %NODEMODULESDIR%\rxjs\add\operator\catch.js %TARGETDIRRXJS%\add\operator\.
copy %NODEMODULESDIR%\rxjs\add\operator\catch.js.map %TARGETDIRRXJS%\add\operator\.
copy %NODEMODULESDIR%\rxjs\add\operator\switchMap.js %TARGETDIRRXJS%\add\operator\.
copy %NODEMODULESDIR%\rxjs\add\operator\switchMap.js.map %TARGETDIRRXJS%\add\operator\.
