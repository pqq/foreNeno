# -------------------------------------------------------
# author:   Frode Klevstul (frode at klevstul dot com)
# started:  17.02.04
# -------------------------------------------------------

def fetchContent(
      foreneno
    , service_id
    , env_id
    , bundle_id
    , fetch_from_cache
    , debug
):
    """
    fetch content from the web, and return it as a dictionary object
    """
    import time

    stream_type         = ''
    cache_use_status    = ''
    db_title            = ''
    feed                = {}
    entries             = {}
    start_time          = time.time()

    # if we are asked to load from cach we'll check the os for a saved file (pickle). we assume that only rss streams are saved as pickles.
    if fetch_from_cache == 'true' and not foreneno.db.stream_source[0].endswith('.json'):
        stream_type = 'rss'
        try:
            feed = foreneno.loadPickle(foreneno.db.filename_feed)
            cache_use_status = 'true'
        except:
            feed = foreneno.loadFeed(foreneno.db.stream_source)
            cache_use_status = 'false (cache not found)'
    else:
        cache_use_status = 'false'

    # direct loading of resource
    if 'false' in cache_use_status:

        # if the stream is a json file
        #   note: we do not support bundled json files. hence we only use the first list element
        if foreneno.db.stream_source[0].endswith(".json"):
            stream_type = 'json'
            json_data = foreneno.loadWebResource(foreneno.db.stream_source[0])
            feed = foreneno.jsonToDictionary(json_data)

        # in all other cases the stream has to be of type rss (otherwise we'll run into problems)
        else:
            stream_type = 'rss'
            feed = foreneno.loadFeed(foreneno.db.stream_source)

    end_time = time.time()

    # store the 'db title' (title set in db.json)
    feed['db_title'] = foreneno.db.stream_title

    # store the number of seconds it took to load the feed
    feed['load_time'] = end_time - start_time

    # store the service_id, env_id and bundle_id in the feed, so they can be used/presented later on
    feed['service_id'] = service_id
    feed['env_id'] = env_id
    feed['bundle_id'] = bundle_id

    # store the actual cache use status, instead of given cache value. so, the result will contain the status telling whether or not cach usage was successfully used
    feed['cache'] = cache_use_status

    # store the stream type, for later use
    feed['stream_type'] = stream_type

    # store debugging information, if requested and on development environment
    if 'true' == debug and 'dev' == env_id:
        feed['debug'] = debug
        feed['debug_object'] = {'service_id': service_id, 'env_id': env_id, 'bundle_id': bundle_id, 'cache_given_param': fetch_from_cache, 'stream_source': foreneno.db.stream_source, 'cache_use_status': cache_use_status, 'stream_type': stream_type}

    return feed, cache_use_status, stream_type


def formatFeed(
      foreneno
    , feed
):
    """
    prepare the feed dictionary, so it is on the correct format. note that we assume that all dictionaries
    of type 'json' are assumed to be correct, and hence just returned. at a later stage, if needed, we can
    implement checking and fixing of json content as well. this would be the place for that logic.
    """
    import hashlib

    return_feed = {}

    # if the stream is of type json it should already be on the right format. hence, no need to do anything.
    if feed['stream_type'] == 'json':
        return_feed = feed

    # the rss stream has to be formatted / converted into foreNeno data
    elif feed['stream_type'] == 'rss':
        hits                    = 0
        result_set              = []

        # loop through all entries, in the default rss formatted feed
        for entry in feed['entries']:
            hits = hits + 1
            new_entry = {}

            # format of the title of the rss feed (note: it is a prerequisite that the title is on exactly this format):
            #   profile | country code | main category | sub category | source | special code | feed id

            if not 'source' in entry:
                entry['source'] = {}

            # if title is set in db.json that title overwrites title given in entry source
            if len(feed['db_title']) > 0:
                entry['source']['title'] = feed['db_title']

            if not 'title' in entry['source']:
                foreneno.error('title is missing from feed', True)

            title_splitted              = entry['source']['title'].split('|')
            feed_profile                = title_splitted[0].rstrip()
            feed_profile_id             = foreneno.asciify(feed_profile.lower())

            try:
                feed_country_code       = title_splitted[1].lstrip().rstrip()
            except:
                feed_country_code       = 'NOTFOUND'

            try:
                feed_main_category      = title_splitted[2].lstrip().rstrip()
                feed_main_category_id   = foreneno.asciify(feed_main_category)
            except:
                feed_main_category      = 'NOTFOUND'
                feed_main_category_id   = feed_main_category

            try:
                feed_sub_category       = title_splitted[3].lstrip().rstrip()
                feed_sub_category_id    = foreneno.asciify(feed_sub_category)
            except:
                feed_sub_category       = 'NOTFOUND'
                feed_sub_category_id    = feed_sub_category

            try:
                feed_source             = title_splitted[4].lstrip().rstrip()
                feed_source_id          = foreneno.asciify(feed_source)
            except:
                feed_source             = 'NOTFOUND'
                feed_source_id          = feed_source

            try:
                feed_special_code       = title_splitted[5].lstrip().rstrip()
            except:
                feed_special_code       = 'NOTFOUND'

            try:
                feed_id                 = title_splitted[6].lstrip().rstrip()
            except:
                feed_id                 = 'NOTFOUND'

            # ids differ in between plain rss and inoreader's rss (ref #437 #451)
            # inoreader id example:
            #   <guid isPermaLink="false">http://www.inoreader.com/article/3a9c6e7eb15eb0b8</guid>
            if 'inoreader.com' in entry['id']:
                this_entry_id           = entry['id'].rsplit('/', 1)[1]
            # blogger.com id examples:
            #   <id>tag:blogger.com,1999:blog-1950818968274202234</id>
            #   <id>tag:blogger.com,1999:blog-1950818968274202234.post-8049782550801938373</id>
            elif 'blogger.com' in entry['id']:
                this_entry_id           = entry['id'].rpartition('-')[2]
            # youtube.com id example:
            #   <id>yt:video:_nXUzwUDeHE</id>
            elif 'yt:video' in entry['id']:
                this_entry_id           = entry['id'].rpartition(':')[2]
            # for all other ids we hash the id string into a unique integer
            else:
                this_entry_id = str(int(hashlib.md5(entry['id']).hexdigest(), 16))

            entry_title                 = entry['title_detail'].value.encode('utf-8')

            # content is stored differently in plain rss than in inoreader's rss (ref #437)
            if 'summary_detail' in entry:
                content_html            = entry['summary_detail'].value.encode('utf-8')
            elif 'content' in entry:
                content_html            = entry['content'][0].value.encode('utf-8')
            else:
                foreneno.error('content not found in entry', True)

            entry_link                  = entry['link']
            published_date              = entry['published']

            # add tags
            # ex:
            # 'tags': [{'label': None, 'scheme': None, 'term': u'jernviljeNo-prod'},
            #          {'label': None, 'scheme': None, 'term': u'jernviljeNo-dev'},
            #          {'label': None, 'scheme': None, 'term': u'jernviljeNo-beta'}]
            tags                        = [];
            if 'tags' in entry:
                for tag in entry['tags']:
                    tags.append(tag['term'])

            new_entry['feed_profile']                   = feed_profile
            new_entry['feed_profile_id']                = feed_profile_id
            new_entry['feed_country_code']              = feed_country_code
            new_entry['feed_main_category']             = feed_main_category
            new_entry['feed_main_category_id']          = feed_main_category_id
            new_entry['feed_sub_category']              = feed_sub_category
            new_entry['feed_sub_category_id']           = feed_sub_category_id
            new_entry['feed_source']                    = feed_source
            new_entry['feed_source_id']                 = feed_source_id
            new_entry['feed_special_code']              = feed_special_code
            new_entry['feed_id']                        = feed_id
            new_entry['entry_title']                    = entry_title
            new_entry['entry_link']                     = entry_link
            new_entry['entry_number']                   = hits
            new_entry['entry_id']                       = this_entry_id
            new_entry['entry_content_text']             = foreneno.htmlToText(content_html)
            new_entry['entry_content_html']             = content_html
            new_entry['entry_tags']                     = tags
            new_entry['entry_published_date']           = published_date
            new_entry['entry_published_date_nerd']      = foreneno.pubDateToNerdDate(published_date, 'nerd')
            new_entry['entry_published_date_display']   = foreneno.pubDateToNerdDate(published_date, 'display')

            result_set.append(new_entry)

        return_feed['cache']            = feed['cache']
        return_feed['hits']             = hits
        return_feed['result_set']       = result_set
        return_feed['load_time']        = feed['load_time']
        return_feed['service_id']       = feed['service_id']
        return_feed['env_id']           = feed['env_id']
        return_feed['bundle_id']        = feed['bundle_id']

        if 'debug' in feed and 'true' in feed['debug']:
                return_feed['debug']                        = feed['debug']
                return_feed['debug_object']                 = feed['debug_object']
                return_feed['debug_object']['hits_initial'] = hits

    else:
        foreneno.error('unknown stream_type ['+feed['stream_type']+']', True)

    return return_feed


def sortFeed(foreneno, feed, sort):
    """
    sort feed, based on sort value given to this function
    """
    import sys

    result_set  = feed['result_set']
    dot_index   = 0

    if sort != None:
        # find the position of the dot ('.'), in the 'sort' parameter
        # and if there is no '.' in the 'sort' parameter set the index at the end (length)
        dot_index = sort.find('.')
        if dot_index == -1:
            dot_index = len(sort)

        try:
            # sorted(iterable[, cmp[, key[, reverse]]])
            # ref: https://docs.python.org/2/library/functions.html#sorted
            # ref: https://docs.python.org/2/howto/sorting.html#sortinghowto

            # sort example values:
            #   - 'feed_profile_id'
            #   - 'feed_profile_id.reverse'
            # 'sort[:dot_index]'                    - the element to sort on (eg 'feed_profile_id')
            # 'key=lambda k: k[sort[:dot_index]]'   - eg 'key=lambda k: k['feed_profile_id']', which means sorting the result set on the profile id
            # 'reverse=('reverse' in sort)'         - becomes true or false, depending on the string 'reverse' being in the 'sort' parameter, or not
            result_set = sorted(result_set, key=lambda k: k[sort[:dot_index]], reverse=('reverse' in sort))
        except KeyError:
            result_set = {'error': 'most likely trying to sort on an element that do not exist ' + format(sys.exc_info()) };
        except:
            result_set = {'error': 'unknown error - ' + format(sys.exc_info()) };

    feed['result_set'] = result_set

    if 'debug' in feed and 'true' in feed['debug']:
        feed['debug_object']['sorting'] = sort

    return feed


def searchFeed(foreneno, feed, search):
    """
    search feed content, based on search parameter given to this function

    prefix codes explained:
    ti - title include
    te - title exclude
    bi - body include
    be - body exclude

    example:    'ti.welcome-ti.frode-be.exclude_me'
    meaning:    include all entries with title that contains the strings 'welcome'
                and 'frode' and where body does not contain 'exclude me'
                (underscore is replaces with space)
    """

    result_set = feed['result_set']
    search_elements = search.split('-')

    if 'debug' in feed and 'true' in feed['debug']:
        feed['debug_object']['search_removals'] = {}

    # loop through all entries (note that i have to use xrange instead for normal 'for x in y' to avoid problems using remove())
    for index in xrange(len(result_set) - 1, -1, -1):
        row = result_set[index]

        # store the title and the body of the entry into variables
        title   = row['entry_title'].lower()
        body    = row['entry_content_text'].lower()

        # loop through all elements given in the search parameter
        for element in search_elements:
            # get the string part of the element, the part after the dot, and replace underscore with space
            string = element[element.find('.')+1:].replace('_', ' ')

            # title include
            if 'ti.' in element:
                if string not in title:
                    feed['result_set'].remove(row)
                    if 'debug' in feed and 'true' in feed['debug']:
                        feed['debug_object']['search_removals'][row['entry_id']] = {'search_value_causing_removal': element, 'entry_title': row['entry_title'], 'entry_published_date_display': row['entry_published_date_display'], 'feed_profile_id': row['feed_profile_id']}
                    break

            # body include
            elif 'bi.' in element:
                if string not in body:
                    feed['result_set'].remove(row)
                    if 'debug' in feed and 'true' in feed['debug']:
                        feed['debug_object']['search_removals'][row['entry_id']] = {'search_value_causing_removal': element, 'entry_title': row['entry_title'], 'entry_published_date_display': row['entry_published_date_display'], 'feed_profile_id': row['feed_profile_id']}
                    break

            # title exclude
            elif 'te.' in element:
                if string in title:
                    feed['result_set'].remove(row)
                    if 'debug' in feed and 'true' in feed['debug']:
                        feed['debug_object']['search_removals'][row['entry_id']] = {'search_value_causing_removal': element, 'entry_title': row['entry_title'], 'entry_published_date_display': row['entry_published_date_display'], 'feed_profile_id': row['feed_profile_id']}
                    break

            # body exclude
            elif 'be.' in element:
                if string in body:
                    feed['result_set'].remove(row)
                    if 'debug' in feed and 'true' in feed['debug']:
                        feed['debug_object']['search_removals'][row['entry_id']] = {'search_value_causing_removal': element, 'entry_title': row['entry_title'], 'entry_published_date_display': row['entry_published_date_display'], 'feed_profile_id': row['feed_profile_id']}
                    break

        if 'debug' in feed and 'true' in feed['debug']:
            feed['debug_object']['search'] = search

    feed['result_set'] = result_set

    return feed


def filterFeed(
      foreneno
    , feed
    , entry_id
    , feed_country_code
    , feed_main_category_id
    , feed_sub_category_id
    , feed_profile_id
    , feed_source_id
    , feed_id
    , max_concurrent_entries
    , exclude
    , avoid_dupes
    , entries_per_main_category
    , entries_per_profile
    , list_size
):
    """
    output content as json
    """
    import json
    from foreneno import Foreneno

    processed_feed                      = {}
    processed_result_set                = []
    hits                                = 0
    feed_previous                       = ''
    entry_id_found                      = False
    entry_id_previous                   = ''
    entry_id_next                       = ''
    concurrent_entries                  = 1
    dupe_control                        = {}
    entries_per_main_category_control   = {}
    entries_per_profile_control         = {}
    filter_debug_object                 = {}                                                                                                # for outputting debugging information we are building this dict with loads of nifty info
    siz_control                         = {}
    hits_per_feed                       = {}

    # loop through the array of entries stored in the result set
    for entry in feed['result_set']:

        # reset value for each loop
        banned_content_found    = False
        fsf_mismatch            = False

        # if an entry is banned
        if entry['entry_id'] in foreneno.db.banned_entries:

            if not 'banned_entry' in filter_debug_object:
                filter_debug_object['banned_entry'] = {}
            filter_debug_object['banned_entry'][entry['entry_id']] = {'entry_title': entry['entry_title']}

            continue

        # if content is banned
        if not 'ws=1' in entry['feed_special_code']:                                                                                        # ignore content ban checks for whitelisted sources (issue #159)

            for banned_string in foreneno.db.banned_title_content:                                                                          # check title
                if (
                        entry['entry_title'].lower().find(banned_string.lower()) >= 0
                    ):
                    banned_content_found = True

                    if not 'banned_title_content' in filter_debug_object:
                        filter_debug_object['banned_title_content'] = {}
                    filter_debug_object['banned_title_content'][entry['entry_id']] = {}
                    filter_debug_object['banned_title_content'][entry['entry_id']]['entry_title'] = entry['entry_title']
                    filter_debug_object['banned_title_content'][entry['entry_id']]['banned_string'] = banned_string

                    break

            for banned_string in foreneno.db.banned_body_content:                                                                           # check body
                if (
                           json.dumps(entry['entry_content_text']).lower().find(banned_string.lower()) >= 0
                        or json.dumps(entry['entry_content_html']).lower().find(banned_string.lower()) >= 0
                    ):
                    banned_content_found = True

                    if not 'banned_body_content' in filter_debug_object:
                        filter_debug_object['banned_body_content'] = {}
                    filter_debug_object['banned_body_content'][entry['entry_id']] = {}
                    filter_debug_object['banned_body_content'][entry['entry_id']]['entry_title'] = entry['entry_title']
                    filter_debug_object['banned_body_content'][entry['entry_id']]['banned_string'] = banned_string

                    break

            if banned_content_found:
                continue

        # if 'feed specific filter' (fsf) value is given as part of the 'feed_special_code'
        if 'fsf=' in entry['feed_special_code']:

            # get the specified fsf parameter from the 'feed_special_code' value for the feed, and split into include and exclude values
            fsf = foreneno.getFeedSpecialCode(entry['feed_special_code'], 'fsf')
            fsf = fsf.replace('_', ' ')                                 # replace underscore with space, as `_` represents space in the fsf value
            fsf_values = fsf.split('-')                                 # split fsf value on '-' into a list of values
            fsf_body_include = []
            fsf_body_exclude = []
            fsf_title_include = []
            fsf_title_exclude = []

            for fsf_value in fsf_values:
                if 'ib.' in fsf_value:
                    fsf_body_include.append(fsf_value[fsf_value.find('.')+1:])
                elif 'eb.' in fsf_value:
                    fsf_body_exclude.append(fsf_value[fsf_value.find('.')+1:])
                elif 'it.' in fsf_value:
                    fsf_title_include.append(fsf_value[fsf_value.find('.')+1:])
                elif 'et.' in fsf_value:
                    fsf_title_exclude.append(fsf_value[fsf_value.find('.')+1:])

            if not 'fsf' in filter_debug_object:
                filter_debug_object['fsf'] = {}
                #filter_debug_object['fsf']['debug'] = {'fsf': fsf, 'feed_special_code': entry['feed_special_code'], 'fsf_body_include': fsf_body_include, 'fsf_body_exclude': fsf_body_exclude, 'fsf_title_include': fsf_title_include, 'fsf_title_exclude': fsf_title_exclude}

            # include body : makes sure feed's include strings exsit in the body
            for include_string in fsf_body_include:
                if json.dumps(entry['entry_content_text']).lower().find(include_string) < 0:
                    fsf_mismatch = True
                    filter_debug_object['fsf'][entry['entry_id']] = {'entry_id': entry['entry_id'], 'entry_title': entry['entry_title'], 'feed_profile_id': entry['feed_profile_id'], 'fsf': fsf}
                    filter_debug_object['fsf'][entry['entry_id']]['fsf_missing_include_string_from_body'] = include_string
                    break

            # exclude body : makes sure feed's exclude strings DON'T exsit in the body
            if not fsf_mismatch:
                for exclude_string in fsf_body_exclude:
                    if json.dumps(entry['entry_content_text']).lower().find(exclude_string) >= 0:
                        fsf_mismatch = True
                        filter_debug_object['fsf'][entry['entry_id']] = {'entry_id': entry['entry_id'], 'entry_title': entry['entry_title'], 'feed_profile_id': entry['feed_profile_id'], 'fsf': fsf}
                        filter_debug_object['fsf'][entry['entry_id']]['fsf_exclude_string_found_in_body'] = exclude_string
                        break

            # include title : makes sure feed's include strings exsit in the title
            if not fsf_mismatch:
                for include_string in fsf_title_include:
                    if entry['entry_title'].lower().find(include_string) < 0:
                        fsf_mismatch = True
                        filter_debug_object['fsf'][entry['entry_id']] = {'entry_id': entry['entry_id'], 'entry_title': entry['entry_title'], 'feed_profile_id': entry['feed_profile_id'], 'fsf': fsf}
                        filter_debug_object['fsf'][entry['entry_id']]['fsf_missing_include_string_from_title'] = include_string
                        break

            # exclude title : makes sure feed's exclude strings DON'T exsit in the title
            if not fsf_mismatch:
                for exclude_string in fsf_title_exclude:
                    if entry['entry_title'].lower().find(exclude_string) >= 0:
                        fsf_mismatch = True
                        filter_debug_object['fsf'][entry['entry_id']] = {'entry_id': entry['entry_id'], 'entry_title': entry['entry_title'], 'feed_profile_id': entry['feed_profile_id'], 'fsf': fsf}
                        filter_debug_object['fsf'][entry['entry_id']]['fsf_exclude_string_found_in_title'] = exclude_string
                        break

            if fsf_mismatch:
                continue

        # if an entry id filter is set
        if entry_id != None:

            # if it is not the entry_id we are looking for...
            if entry_id != entry['entry_id']:

                # store debugging information
                if not 'entry_id' in filter_debug_object:
                    filter_debug_object['entry_id'] = {}
                filter_debug_object['entry_id'][entry['entry_id']] = {'entry_id': entry['entry_id'], 'entry_title': entry['entry_title'], 'feed_profile_id': entry['feed_profile_id']}

                # scenario one: it is an entry id prior to the one we are looking for
                if not entry_id_found:
                    entry_id_previous = entry['entry_id']

                    # since it is not the entry we are searching for we jump to the next one in the loop, and hence avoid keeping it
                    continue

                # scenario two: it is the entry id following the one we are looking for
                elif not entry_id_next:
                    entry_id_next = entry['entry_id']

            # if it is the entry_id we are searching for...
            else:
                entry_id_found = True

            # if we have a previous entry id stored we add it to this entry
            if entry_id_previous:
                entry['entry_id_previous'] = entry_id_previous

            # if this is 'the next entry' we update the previously found entry with info pointing to this id
            if entry_id_next:
                processed_result_set[0]['entry_id_next'] = entry_id_next
                continue

        # if a country code filter is set
        if feed_country_code != None:
            if feed_country_code != entry['feed_country_code']:
                if not 'feed_country_code' in filter_debug_object:
                    filter_debug_object['feed_country_code'] = {}
                filter_debug_object['feed_country_code'][entry['entry_id']] = {'feed_country_code': entry['feed_country_code'], 'entry_title': entry['entry_title'], 'feed_profile_id': entry['feed_profile_id']}
                continue

        # if a main category filter is set
        if feed_main_category_id != None:
            if feed_main_category_id != entry['feed_main_category_id']:
                if not 'feed_main_category_id' in filter_debug_object:
                    filter_debug_object['feed_main_category_id'] = {}
                filter_debug_object['feed_main_category_id'][entry['entry_id']] = {'feed_main_category_id': entry['feed_main_category_id'], 'entry_title': entry['entry_title'], 'feed_profile_id': entry['feed_profile_id']}
                continue

        # if a sub category filter is set
        if feed_sub_category_id != None:
            if feed_sub_category_id != entry['feed_sub_category_id']:
                if not 'feed_sub_category_id' in filter_debug_object:
                    filter_debug_object['feed_sub_category_id'] = {}
                filter_debug_object['feed_sub_category_id'][entry['entry_id']] = {'feed_sub_category_id': entry['feed_sub_category_id'], 'entry_title': entry['entry_title'], 'feed_profile_id': entry['feed_profile_id']}
                continue

        # if a profile filter is set
        if feed_profile_id != None:
            if feed_profile_id != entry['feed_profile_id']:
                if not 'feed_profile_id' in filter_debug_object:
                    filter_debug_object['feed_profile_id'] = {}
                filter_debug_object['feed_profile_id'][entry['entry_id']] = {'entry_title': entry['entry_title'], 'feed_profile_id': entry['feed_profile_id']}
                continue

        # if a feed source filter is set
        if feed_source_id != None:
            if feed_source_id != entry['feed_source_id']:
                if not 'feed_source_id' in filter_debug_object:
                    filter_debug_object['feed_source_id'] = {}
                filter_debug_object['feed_source_id'][entry['entry_id']] = {'feed_source_id': entry['feed_source_id'], 'entry_title': entry['entry_title'], 'feed_profile_id': entry['feed_profile_id']}
                continue

        # if a feed id filter is set
        if feed_id != None:
            if feed_id != entry['feed_id']:
                if not 'feed_id' in filter_debug_object:
                    filter_debug_object['feed_id'] = {}
                filter_debug_object['feed_id'][entry['entry_id']] = {'feed_id': entry['feed_id'], 'entry_title': entry['entry_title'], 'feed_profile_id': entry['feed_profile_id']}
                continue

        # if a 'max_concurrent_entries' (entries from same source, in a row) filter is set
        if max_concurrent_entries != None:
            if feed_previous == entry['feed_id']:
                if concurrent_entries >= int(max_concurrent_entries):

                    if not 'max_concurrent_entries' in filter_debug_object:
                        filter_debug_object['max_concurrent_entries'] = {}
                    filter_debug_object['max_concurrent_entries'][entry['entry_id']] = {'feed_id': entry['feed_id'], 'feed_source': entry['feed_source'], 'entry_title': entry['entry_title'], 'entry_published_date_display': entry['entry_published_date_display'], 'feed_profile_id': entry['feed_profile_id']}

                    continue
                else:
                    concurrent_entries = concurrent_entries + 1

        # if an exclude filter is set
        if exclude != None:

            if not 'exclude' in filter_debug_object:
                filter_debug_object['exclude'] = {}
                filter_debug_object['exclude']['exclude_string'] = exclude

            if 'feed_country_code' in exclude:
                if 'feed_country_code.' + entry['feed_country_code'] in exclude:
                    filter_debug_object['exclude'][entry['entry_id']] = {'exclude_hit': 'feed_country_code=' + entry['feed_country_code'], 'entry_title': entry['entry_title'], 'entry_published_date_display': entry['entry_published_date_display'], 'feed_profile_id': entry['feed_profile_id']}
                    continue
            if 'feed_id' in exclude:
                if 'feed_id.' + entry['feed_id'] in exclude:
                    filter_debug_object['exclude'][entry['entry_id']] = {'exclude_hit': 'feed_id=' + entry['feed_id'], 'entry_title': entry['entry_title'], 'entry_published_date_display': entry['entry_published_date_display'], 'feed_profile_id': entry['feed_profile_id']}
                    continue
            if 'feed_main_category_id' in exclude:
                if 'feed_main_category_id.' + entry['feed_main_category_id'] in exclude:
                    filter_debug_object['exclude'][entry['entry_id']] = {'exclude_hit': 'feed_main_category_id=' + entry['feed_main_category_id'], 'entry_title': entry['entry_title'], 'entry_published_date_display': entry['entry_published_date_display'], 'feed_profile_id': entry['feed_profile_id']}
                    continue
            if 'feed_profile_id' in exclude:
                if 'feed_profile_id.' + entry['feed_profile_id'] in exclude:
                    filter_debug_object['exclude'][entry['entry_id']] = {'exclude_hit': 'feed_profile_id=' + entry['feed_profile_id'], 'entry_title': entry['entry_title'], 'entry_published_date_display': entry['entry_published_date_display'], 'feed_profile_id': entry['feed_profile_id']}
                    continue
            if 'feed_source_id' in exclude:
                if 'feed_source_id.' + entry['feed_source_id'] in exclude:
                    filter_debug_object['exclude'][entry['entry_id']] = {'exclude_hit': 'feed_source_id=' + entry['feed_source_id'], 'entry_title': entry['entry_title'], 'entry_published_date_display': entry['entry_published_date_display'], 'feed_profile_id': entry['feed_profile_id']}
                    continue
            if 'feed_sub_category_id' in exclude:
                if 'feed_sub_category_id.' + entry['feed_sub_category_id'] in exclude:
                    filter_debug_object['exclude'][entry['entry_id']] = {'exclude_hit': 'feed_sub_category_id=' + entry['feed_sub_category_id'], 'entry_title': entry['entry_title'], 'entry_published_date_display': entry['entry_published_date_display'], 'feed_profile_id': entry['feed_profile_id']}
                    continue

        # if a dupe filter is set
        if avoid_dupes != None:

            if not 'avoid_dupes' in filter_debug_object:
                filter_debug_object['avoid_dupes'] = {}
                filter_debug_object['avoid_dupes']['avoid_string'] = avoid_dupes

            # title check
            if 'entry_title' in avoid_dupes:                                                                                                # check if we shall do a dupe control of entry titles
                if 'entry_title' in dupe_control:                                                                                           # ... check if the entry title element has been added to our dupe_control dict
                    if entry['entry_title'] in dupe_control['entry_title']:                                                                 # ... ... check if current title (entry_title) has been read (and stored in dupe_control) before
                        filter_debug_object['avoid_dupes'][entry['entry_id']] = {'dupe_hit': 'entry_title', 'entry_title': entry['entry_title'], 'entry_published_date_display': entry['entry_published_date_display'], 'feed_profile_id': entry['feed_profile_id']}
                        continue                                                                                                            # ... ... ... if it has, skip this one
                    else:                                                                                                                   #
                        dupe_control['entry_title'].append(entry['entry_title']);                                                           # ... ... ... if not add the title to our list of entry titles
                else:                                                                                                                       # ... else if we do not have an entry_title element in our dupe_control dict(ionary)
                    dupe_control.update({'entry_title' : []});                                                                              # ... ... we add a element named 'entry_title', which relates to a list. this way we can create our own associative array / hash
                    dupe_control['entry_title'].append(entry['entry_title'])                                                                # ... ... and we add the current title to the empty list of entry titles

            # link check
            if 'entry_link' in avoid_dupes:                                                                                                 # check if we shall do a dupe control of entry links
                if 'entry_link' in dupe_control:
                    if entry['entry_link'] in dupe_control['entry_link']:
                        filter_debug_object['avoid_dupes'][entry['entry_id']] = {'dupe_hit': 'entry_link', 'entry_title': entry['entry_title'], 'entry_published_date_display': entry['entry_published_date_display'], 'feed_profile_id': entry['feed_profile_id']}
                        continue
                    else:
                        dupe_control['entry_link'].append(entry['entry_link']);
                else:
                    dupe_control.update({'entry_link' : []});
                    dupe_control['entry_link'].append(entry['entry_link'])

        # if a 'entries_per_main_category' (limit to how many entries per main category) filter is set
        if entries_per_main_category != None:
            if entry['feed_main_category'] in entries_per_main_category_control:
                if entries_per_main_category_control[entry['feed_main_category']] < int(entries_per_main_category):
                    entries_per_main_category_control[entry['feed_main_category']] = entries_per_main_category_control[entry['feed_main_category']] + 1
                else:
                    if not 'entries_per_main_category' in filter_debug_object:
                        filter_debug_object['entries_per_main_category'] = {}
                    filter_debug_object['entries_per_main_category'][entry['entry_id']] = {'count_limit_reached': entries_per_main_category, 'entry_title': entry['entry_title'], 'entry_published_date_display': entry['entry_published_date_display'], 'feed_profile_id': entry['feed_profile_id']}
                    continue
            else:
                entries_per_main_category_control[entry['feed_main_category']] = 1

        # if a 'entries_per_profile' (limit to how many entries per profile) filter is set
        if entries_per_profile != None:
            if entry['feed_profile_id'] in entries_per_profile_control:
                if entries_per_profile_control[entry['feed_profile_id']] < int(entries_per_profile):
                    entries_per_profile_control[entry['feed_profile_id']] = entries_per_profile_control[entry['feed_profile_id']] + 1
                else:
                    if not 'entries_per_profile' in filter_debug_object:
                        filter_debug_object['entries_per_profile'] = {}
                    filter_debug_object['entries_per_profile'][entry['entry_id']] = {'count_limit_reached': entries_per_profile, 'entry_title': entry['entry_title'], 'entry_published_date_display': entry['entry_published_date_display'], 'feed_profile_id': entry['feed_profile_id']}
                    continue
            else:
                entries_per_profile_control[entry['feed_profile_id']] = 1

        # if size (siz) is set in `feed_special_code`
        if 'siz=' in entry['feed_special_code']:

            # get siz from special code if we already haven't done so
            if not entry['feed_id'] in siz_control:
                siz_control[entry['feed_id']] = int(foreneno.getFeedSpecialCode(entry['feed_special_code'], 'siz'))

            # make initial hits_per_feed entry
            if not entry['feed_id'] in hits_per_feed:
                hits_per_feed[entry['feed_id']] = 1
            # ... or increase the count by one if it already exsists
            else:
                hits_per_feed[entry['feed_id']] = hits_per_feed[entry['feed_id']] + 1

            # if we have reached the limit (check if hits is bigger than, instead of 'bigger or equal' since we already have added the current entry)
            if hits_per_feed[entry['feed_id']] > siz_control[entry['feed_id']]:
                # build debug dict
                if not 'feed_special_code.siz' in filter_debug_object:
                    filter_debug_object['feed_special_code.siz'] = {}
                filter_debug_object['feed_special_code.siz'][entry['entry_id']] = {'entry_title': entry['entry_title'], 'entry_published_date_display': entry['entry_published_date_display'], 'feed_profile_id': entry['feed_profile_id'], 'feed_id': entry['feed_id'], 'siz_control['+entry['feed_id']+']': siz_control[entry['feed_id']], 'hits_per_feed['+entry['feed_id']+']': hits_per_feed[entry['feed_id']]}

                # continue to the next cycle/entry in the loop as the limit has been reached
                continue

        # if a list size filter is set as a get parameter
        if list_size != None:

            if hits >= int(list_size):
                if not 'list_size' in filter_debug_object:
                    filter_debug_object['list_size'] = {}
                filter_debug_object['list_size'][entry['entry_id']] = {'count_limit_reached': list_size, 'entry_title': entry['entry_title'], 'entry_published_date_display': entry['entry_published_date_display'], 'feed_profile_id': entry['feed_profile_id']}
                continue

        processed_result_set.append(entry)
        feed_previous = entry['feed_id']
        hits = hits + 1

    # build a new dictionary
    processed_feed['cache'] = feed['cache']
    try:
        processed_feed['generator']     = feed['generator']
    except:
        processed_feed['generator']     = 'foreNeno.rpc'
    try:
        processed_feed['debug']                             = feed['debug']
        processed_feed['debug_object']                      = feed['debug_object']
        processed_feed['debug_object']['hits_after_filter'] = hits
        processed_feed['debug_object']['filter_result']     = filter_debug_object
    except:
        pass
    processed_feed['hits']              = hits
    processed_feed['load_time']         = feed['load_time']
    processed_feed['service_id']        = feed['service_id']
    processed_feed['env_id']            = feed['env_id']
    processed_feed['bundle_id']         = feed['bundle_id']
    processed_feed['result_set']        = processed_result_set

    #return feed
    return processed_feed


def coreFilter(
      foreneno
    , feed
):
    """
    filter the core content, to make sure we only present data for given service id
    """

    # get data from the feed
    result_set      = feed['result_set']
    service_id      = feed['service_id']

    for index in xrange(len(result_set) - 1, -1, -1):
        row = result_set[index]

        # delete rows from the original result set which are for other service ids than what was requested
        if service_id not in row:
            feed['result_set'].remove(row)

    # add generator value to feed
    feed['generator'] = 'foreNeno.rpc'

    return feed


def outputFeed(
      foreneno
    , feed
    , method
    , callback
):
    """
    output feed
    """
    import json
    import datetime

    # get the result set, or the array of posts
    result_set = feed['result_set']

    # number of hits has do be updated, as we are here changing the result set
    hits = 0

    # printing the content type : https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Type
    if '_json' in method:
        print 'Content-Type: application/json; charset="UTF-8"'

        # on local dev environment the rpc is running on a different web server than the web app, hence all origins has to be allowed
        if foreneno.getEnv() == 'dev':
            print 'Access-Control-Allow-Origin: *'

        # the required empty line, after the response headers
        print ''

    elif '_rss' in method:
        # ----
        # feed validator:
        # https://validator.w3.org/feed/
        # ----
        print 'Content-Type: application/rss+xml; charset="UTF-8"'
        print ''
        print '<?xml version="1.0" encoding="UTF-8" ?>'
        print '<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">'
        print '<channel>'
        print '<title>' + feed['service_id'] + ' feed</title>'
        print '<link>' + foreneno.db.base_url + '</link>'
        print '<description>foreNeno feed, generated by foreNeno web bundling engine</description>'
        # the validator returns a warning when the date equals sysdate. going two hours back in time fixes this. however, going through feedburner this should be no problem.
        print '<lastBuildDate>' + str(datetime.datetime.now().strftime('%a, %d %b %Y %H:%M:%S %z')) + '+0100</lastBuildDate>'
        print '<generator>foreNeno RSS generator</generator>'
        print '<docs>http://blogs.law.harvard.edu/tech/rss</docs>'
        print '<language>en-us</language>'
        print '<copyright>Copyright foreNeno.com</copyright>'
        print '<ttl>30</ttl>'
        # using parameters in url seems to be the reason for validator fail for the above url
        # print '<atom:link href="' + foreneno.db.base_url + '/cgi/rpc.py?' + foreneno.getQueryString() + '" rel="self" type="application/rss+xml" />'
        # ... that is no problem though, as using the feedburner feed is the correct thing to do
        # for the dev feed the validator returns the error "Self reference doesn't match document location". this should be no problem with the prod feed.
        print '<atom:link href="http://feeds.feedburner.com/' + foreneno.db.service_id + '" rel="self" type="application/rss+xml" />'
    else:
        print 'Content-Type: text/plain; charset="UTF-8"'
        print ''

    # print name of callback function, if any...
    if callback != None:
        print callback + ' ('

    # note:
    #   to find the optimal way of filtering away unwanted content a question was asked on stackoverflow:
    #   http://stackoverflow.com/questions/42318698/how-to-filter-out-elements-from-a-list-of-dictionaries-with-repeating-keys-and-d

    # output all, so nothing is to be filtered away. this method is nifty for debugging.
    if method == 'all_json':
        if 'hits' in feed: hits = feed['hits']

    # list all entries - json and rss
    elif method == 'list_entries_json' or method == 'list_entries_rss':

        # the most efficient way of outputting just the data we want is here to delete the data we do not want.
        for row in result_set:
            hits = hits + 1
            if 'foreneno_cms' in row: del row['foreneno_cms']

        # an alternative method, to the above deletion method, would be doing list comprehension:
        #     processed_result_set = [{
        #           'entry_content_html':             row['entry_content_html']
        #         , 'entry_content_text':             row['entry_content_text']
        #         , 'entry_id':                       row['entry_id']
        #         , 'entry_link':                     row['entry_link']
        #         , 'entry_number':                   row['entry_number']
        #         , 'entry_published_date':           row['entry_published_date']
        #         , 'entry_published_date_display':   row['entry_published_date_display']
        #         , 'entry_published_date_nerd':      row['entry_published_date_nerd']
        #         , 'entry_tags':                     row['entry_tags']
        #         , 'entry_title':                    row['entry_title']
        #         , 'feed_country_code':              row['feed_country_code']
        #         , 'feed_id':                        row['feed_id']
        #         , 'feed_main_category':             row['feed_main_category']
        #         , 'feed_main_category_id':          row['feed_main_category_id']
        #         , 'feed_profile':                   row['feed_profile']
        #         , 'feed_profile_id':                row['feed_profile_id']
        #         , 'feed_source':                    row['feed_source']
        #         , 'feed_special_code':              row['feed_special_code']
        #         , 'feed_sub_category':              row['feed_sub_category']
        #         , 'feed_sub_category_id':           row['feed_sub_category_id']
        #         , 'service_id':                    row['service_id']
        #         , 'env_id':                        row['env_id']
        #     } for row in result_set]

    # list all feeds
    elif method == 'list_feeds_json':
        uniqueness_check = []

        # if the result set contains error message we do nothing
        if not 'error' in feed['result_set']:

            # for filtering out unwanted duplicates it is more efficient to drop existing entries from
            # the list, than creating a new copy with only wanted data. the challenge is to remove an
            # entry from a list while iterating through the elements. if iterating the "common way",
            # like: 'for element in list_of_elements', doing 'list_of_elements.remove(element)' will
            # not work correctly (there will be elements left, unparsed).
            # two pages on stackexchange helped me in finding a good way of dealing with this issue:
            # - http://stackoverflow.com/questions/1207406/remove-items-from-a-list-while-iterating
            # - http://stackoverflow.com/questions/6022764/python-removing-list-element-while-iterating-over-list
            # below i have ended up choosing to iterate backwards, through the list, instead of creating a
            # copy of the list, like: 'for element in list(list_of_elements)'

            # xrange(start, stop[, step])
            #   https://docs.python.org/2/library/functions.html#xrange
            for index in xrange(len(result_set) - 1, -1, -1):
                row = result_set[index]

                # never present the same feed twice
                if row['feed_id'] in uniqueness_check:
                    result_set.remove(row)
                    continue
                else:
                    hits = hits + 1
                    uniqueness_check.append(row['feed_id'])

                # check to see if the feed is valid
                feed_status = 'invalid'
                if all(x != 'NOTFOUND' for x in (row['feed_country_code'], row['feed_main_category'], row['feed_sub_category'], row['feed_source'], row['feed_special_code'], row['feed_id'])):
                    feed_status = 'valid'

                # update the result set
                row['feed_status'] = feed_status
                del row['entry_content_html']
                del row['entry_content_text']
                del row['entry_id']
                del row['entry_link']
                del row['entry_number']
                del row['entry_published_date']
                del row['entry_published_date_display']
                del row['entry_published_date_nerd']
                del row['entry_tags']
                del row['entry_title']
                if 'foreneno_cms' in row: del row['foreneno_cms']

    # list all profiles
    elif method == 'list_profiles_json':
        uniqueness_check = []

        if not 'error' in feed['result_set']:

            for index in xrange(len(result_set) - 1, -1, -1):
                row = result_set[index]

                if row['feed_profile_id'] in uniqueness_check:
                    result_set.remove(row)
                    continue
                else:
                    hits = hits + 1
                    uniqueness_check.append(row['feed_profile_id'])

                del row['entry_content_html']
                del row['entry_content_text']
                del row['entry_id']
                del row['entry_link']
                del row['entry_number']
                del row['entry_published_date']
                del row['entry_published_date_display']
                del row['entry_published_date_nerd']
                del row['entry_tags']
                del row['entry_title']
                del row['feed_id']
                del row['feed_source']
                del row['feed_source_id']
                del row['feed_special_code']
                if 'foreneno_cms' in row: del row['foreneno_cms']

    # list all countries
    elif method == 'list_countries_json':
        uniqueness_check = []

        if not 'error' in feed['result_set']:

            for index in xrange(len(result_set) - 1, -1, -1):
                row = result_set[index]

                if row['feed_country_code'] in uniqueness_check:
                    result_set.remove(row)
                    continue
                else:
                    hits = hits + 1
                    uniqueness_check.append(row['feed_country_code'])

                del row['entry_content_html']
                del row['entry_content_text']
                del row['entry_id']
                del row['entry_link']
                del row['entry_number']
                del row['entry_published_date']
                del row['entry_published_date_display']
                del row['entry_published_date_nerd']
                del row['entry_tags']
                del row['entry_title']
                # del row['feed_country_code']
                del row['feed_id']
                del row['feed_main_category']
                del row['feed_main_category_id']
                del row['feed_profile']
                del row['feed_profile_id']
                del row['feed_source']
                del row['feed_source_id']
                del row['feed_special_code']
                del row['feed_sub_category']
                del row['feed_sub_category_id']
                if 'foreneno_cms' in row: del row['foreneno_cms']

    # list all main categories
    elif method == 'list_maincategories_json':
        uniqueness_check = []

        if not 'error' in feed['result_set']:

            for index in xrange(len(result_set) - 1, -1, -1):
                row = result_set[index]

                if row['feed_main_category'] in uniqueness_check:
                    result_set.remove(row)
                    continue
                else:
                    hits = hits + 1
                    uniqueness_check.append(row['feed_main_category'])

                del row['entry_content_html']
                del row['entry_content_text']
                del row['entry_id']
                del row['entry_link']
                del row['entry_number']
                del row['entry_published_date']
                del row['entry_published_date_display']
                del row['entry_published_date_nerd']
                del row['entry_tags']
                del row['entry_title']
                del row['feed_country_code']
                del row['feed_id']
                # del row['feed_main_category']
                # del row['feed_main_category_id']
                del row['feed_profile']
                del row['feed_profile_id']
                del row['feed_source']
                del row['feed_source_id']
                del row['feed_special_code']
                del row['feed_sub_category']
                del row['feed_sub_category_id']
                if 'foreneno_cms' in row: del row['foreneno_cms']

    # list all sub categories
    elif method == 'list_subcategories_json':
        uniqueness_check = []

        if not 'error' in feed['result_set']:

            for index in xrange(len(result_set) - 1, -1, -1):
                row = result_set[index]

                if row['feed_sub_category'] in uniqueness_check:
                    result_set.remove(row)
                    continue
                else:
                    hits = hits + 1
                    uniqueness_check.append(row['feed_sub_category'])

                del row['entry_content_html']
                del row['entry_content_text']
                del row['entry_id']
                del row['entry_link']
                del row['entry_number']
                del row['entry_published_date']
                del row['entry_published_date_display']
                del row['entry_published_date_nerd']
                del row['entry_tags']
                del row['entry_title']
                del row['feed_country_code']
                del row['feed_id']
                del row['feed_main_category']
                del row['feed_main_category_id']
                del row['feed_profile']
                del row['feed_profile_id']
                del row['feed_source']
                del row['feed_source_id']
                del row['feed_special_code']
                # del row['feed_sub_category']
                # del row['feed_sub_category_id']
                if 'foreneno_cms' in row: del row['foreneno_cms']

    # unrecognised method
    else:
        foreneno.error('unknown method ['+method+']', True)

    # update the result set
    feed['result_set'] = result_set

    # update hits
    feed['hits'] = hits

    if '_json' in method:

        # output feed as nicely formatted json data
        print json.dumps(feed, sort_keys=True, indent=4, separators=(',', ': '), encoding="utf-8")

    # rss feed validator: https://validator.w3.org/feed/#validate_by_input
    # overview of rss elements: http://www.feedvalidator.org/docs/rss2.html
    elif '_rss' in method:
        if method == 'list_entries_rss':
            for row in result_set:
                print '<item>'
                print '    <title>' + row['entry_title'] + '</title>'
                # on purpose all users are sent to the front page of the service, instead of the entry itself.
                # this is done as i want users to use the site instead of a feedreader for checking news.
                # in other words an annoying rss limitation is added on purpose
                # correct entry links:
                #print '    <guid>' + foreneno.db.base_url + '/page/entry/entry_id/' + row['entry_id'] + '</guid>'
                #print '    <link>' + foreneno.db.base_url + '/page/entry/entry_id/' + row['entry_id'] + '</link>'
                # taking users to the fron page:
                print '    <guid>' + foreneno.db.base_url + '</guid>'
                print '    <link>' + foreneno.db.base_url + '</link>'
                print '    <description>' + row['entry_content_text'][:500].replace('\n', '').replace('\r','') + '... </description>'
                print '    <pubDate>' + row['entry_published_date'] + '</pubDate>'
                print '</item>'

            print '</channel>'
            print '</rss>'

    else:
        foreneno.error('unknown method type ['+method+']')

    # (possibly) end the callback function
    if callback != None:
        print ' );'


def getCgiParameters():
    """
    get parameters sent along with web request
    """
    import cgi

    form = cgi.FieldStorage()
    parameters =(
        {
               'service_id'                  : form.getvalue('service_id')      or 'atestservice'               # @ENV_DEV                  # atestservice - forenenocom - jernviljeno - runnersno - thisworldis
#@             'service_id'                  : form.getvalue('service_id')                                      # @ENV_BETA @ENV_PROD
             , 'env_id'                      : form.getvalue('env_id')          or 'dev'                        # @ENV_DEV                  # dev - beta - prod
#@           , 'env_id'                      : form.getvalue('env_id')          or ''                           # @ENV_BETA @ENV_PROD
             , 'bundle_id'                   : form.getvalue('bundle_id')       or ''                           #                           # eg: core.about - core.cfg
             , 'method'                      : form.getvalue('method')          or ''                           # @ENV_DEV                  # eg: list_entries_json
#@           , 'method'                      : form.getvalue('method')                                          # @ENV_BETA @ENV_PROD
             , 'callback'                    : form.getvalue('callback')
             , 'cache'                       : form.getvalue('cache')           or 'false'
             , 'entry_id'                    : form.getvalue('entry_id')
             , 'feed_country_code'           : form.getvalue('feed_country_code')
             , 'feed_main_category_id'       : form.getvalue('feed_main_category_id')
             , 'feed_sub_category_id'        : form.getvalue('feed_sub_category_id')
             , 'feed_profile_id'             : form.getvalue('feed_profile_id')
             , 'feed_source_id'              : form.getvalue('feed_source_id')
             , 'feed_id'                     : form.getvalue('feed_id')
             , 'max_concurrent_entries'      : form.getvalue('max_concurrent_entries')
             , 'exclude'                     : form.getvalue('exclude')
             , 'avoid_dupes'                 : form.getvalue('avoid_dupes')
             , 'entries_per_main_category'   : form.getvalue('entries_per_main_category')
             , 'entries_per_profile'         : form.getvalue('entries_per_profile')
             , 'list_size'                   : form.getvalue('list_size')
             , 'sort'                        : form.getvalue('sort')            # or 'feed_title'
             , 'search'                      : form.getvalue('search')          # or 'te.excludeme-te.allsid-bi.uker-be.celler'
             , 'debug'                       : form.getvalue('debug')           or 'false'
        }
    )

    return parameters


def main():
    """
    where it all begins...
    """
    from foreneno import Foreneno

    params                      = getCgiParameters()
    service_id                  = params['service_id']
    env_id                      = params['env_id']
    bundle_id                   = params['bundle_id']
    method                      = params['method']
    callback                    = params['callback']
    cache                       = params['cache']
    entry_id                    = params['entry_id']
    feed_country_code           = params['feed_country_code']
    feed_main_category_id       = params['feed_main_category_id']
    feed_sub_category_id        = params['feed_sub_category_id']
    feed_profile_id             = params['feed_profile_id']
    feed_source_id              = params['feed_source_id']
    feed_id                     = params['feed_id']
    max_concurrent_entries      = params['max_concurrent_entries']
    exclude                     = params['exclude']
    avoid_dupes                 = params['avoid_dupes']
    entries_per_main_category   = params['entries_per_main_category']
    entries_per_profile         = params['entries_per_profile']
    list_size                   = params['list_size']
    sort                        = params['sort']
    search                      = params['search']
    debug                       = params['debug']

    foreneno                    = Foreneno(service_id, env_id, bundle_id)

    feed                        = {}

    # check to make sure we can continue...
    if not foreneno.db.initialised:
        foreneno.error('foreneno.db has not been initialised. likely due to missing service_id or env_id', True)

    # fetch content from a resource on the internet
    feed, cache_use_status, stream_type = fetchContent(
          foreneno
        , service_id
        , env_id
        , bundle_id
        , cache
        , debug
    )

    # operations for core data
    if 'core' in bundle_id:

        # filter out content for given service
        feed = coreFilter(
              foreneno
            , feed
        )

    # operations for content data
    else:

        # do some initially preprocessing, for make sure the content is on the right format
        feed = formatFeed(
              foreneno
            , feed
        )

        # sorting has to be done before filtering
        feed = sortFeed(
              foreneno
            , feed
            , sort
        )

        # searching has to be done before filtering
        if search:
            feed = searchFeed(
                  foreneno
                , feed
                , search
            )

        # the sort function above might return an error (feed['result_set']['error']). if it does, do not filter feed before outputting it
        if not 'error' in feed['result_set']:

            # filter feed
            feed = filterFeed(
                  foreneno
                , feed
                , entry_id
                , feed_country_code
                , feed_main_category_id
                , feed_sub_category_id
                , feed_profile_id
                , feed_source_id
                , feed_id
                , max_concurrent_entries
                , exclude
                , avoid_dupes
                , entries_per_main_category
                , entries_per_profile
                , list_size
            )

    outputFeed(
          foreneno
        , feed
        , method
        , callback
    )


if '__main__' == __name__:
    main()
