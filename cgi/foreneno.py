# -------------------------------------------------------
# author:   Frode Klevstul (frode at klevstul dot com)
# started:  16.03.12
# -------------------------------------------------------


class Foreneno:
    """
    main class
    """

    name        = 'foreNeno'
    db          = None


    def __init__(self, service_id=None, env_id=None, bundle_id=None):
        self.db = self.Database()
        self.db.initialised = False

        if service_id and env_id:
            self.initiateDatabase(self.db, service_id, env_id, bundle_id)
        # else: the initiateDatabase() function has to be called after creating a new Foreneno instance, if needed that is


    class Database:
        """
        class that stores all information needed for the program to run.
        this class becomes the program's database
        """

        pass


    def initiateDatabase(self, db, service_id, env_id, bundle_id=None):
        """
        load database with data
        """
        import json

        # value used for checking status of database initialisation
        db.initialised = True

        # storing data used for initialising for further usage
        db.service_id   = service_id
        db.env_id       = env_id
        db.bundle_id    = bundle_id

        # build a string with service_id, bundle_id and env_id
        # ref: https://stackoverflow.com/questions/8626694/join-4-strings-to-the-one-if-they-are-not-empty-in-python
        service_bundle_env_id = '.'.join(filter(None, [service_id, bundle_id, env_id]))

        # set path to database directory
        if 'dev' == env_id:
            base_database_path = 'R:/googleDrive/projects/_gitHub/foreNeno/database'
        elif 'beta' == env_id:
            base_database_path = '/var/www/foreneno.com/foreNeno/beta'
        elif 'prod' == env_id:
            base_database_path = '/var/www/foreneno.com/foreNeno/prod/latest'
        else:
            self.error('unknown environment ['+env_id+']', True)

        # load feed urls
        f = open(base_database_path +'/db.json', 'r')
        dbContent = json.load(f)
        f.close()

        # get the service's base_url. this is (currently) only used when outputting the rss feed
        try:
            db.base_url  = dbContent[service_id]['baseurl'][env_id]
        except:
            self.error('no base url found for service_id ['+service_id+'] with env_id [' + env_id + ']', True)

        # get the url to the resource on the web where core or content sources are found
        try:
            if (bundle_id and 'core.' in bundle_id):
                db.sources_core_content_url = dbContent[service_id]['sourcescoreurl'][env_id]
            else:
                db.sources_core_content_url = dbContent[service_id]['sourcescontenturl'][env_id]
        except:
            self.error('no url found for core/sources, using service_id ['+service_id+'], env_id ['+env_id+'] and bundle_id ['+str(bundle_id)+']', True)

        # load content sources from web
        content_sources = self.jsonToDictionary(self.loadWebResource(db.sources_core_content_url))

        # get the stream_source, the web resource where to load the content data, or entries, from
        try:
            db.stream_source  = content_sources[service_bundle_env_id]
        except:
            try:
                # in case loading of the environment specific service id failed we try the default one (issue #296)
                default_stream_source = service_bundle_env_id[:service_bundle_env_id.rfind('.')] + '.default'
                db.stream_source  = content_sources[default_stream_source]
            except:
                self.errorAsJson('could not find stream source for service ['+service_id+'] with name ['+service_bundle_env_id+'] nor a default stream source ['+default_stream_source+'] in resource ['+db.sources_core_content_url+']', True)

        # process stream_source
        #   example formats:
        #       atestservice.default":             "http://www.inoreader.com/stream/user/1005611799/tag/atestservice?dt=pub&n=100
        #       atestservice.fno.default":         "http://foreneno.blogspot.com/feeds/posts/default       title={foreneno.com | eng | service | news | blog | ff=1 gts=0 ws=1 pli=0 edd=0 | 1709231326}
        #       atestservice.fno.default":         "bundle={http://foreneno.blogspot.com/feeds/posts/default, https://websta.me/rss/n/forene.no?t=3}       title={foreneno.com | eng | service | news | blog | ff=1 gts=0 ws=1 pli=0 edd=0 | 1709231326}
        if 'title={' in db.stream_source:
            db.stream_title = db.stream_source.split(' title={')[1][:-1]

            if 'bundle={' in db.stream_source:
                db.stream_source = db.stream_source.replace(' ', '', 999).split('bundle={')[1].split('}')[0].split(',')
            else:
                db.stream_source = [db.stream_source.split(' title={')[0]]
        else:
            db.stream_title = ''
            db.stream_source = [db.stream_source]

        # load banned entries
        db.banned_entries           = []
        file_name                   = base_database_path + '/bannedEntries.txt'
        with open(file_name) as f:
            list_of_lines = f.read().splitlines()

            for line in list_of_lines:
                if not line.startswith('#'):
                    line = line.split('#', 1)[0].rstrip()
                    db.banned_entries.append(line)
        f.close()

        # load banned title content
        db.banned_title_content     = []
        file_name                   = base_database_path + '/bannedTitleContent.txt'
        with open(file_name) as f:
            list_of_lines = f.read().splitlines()

            for line in list_of_lines:
                if not line.startswith('#'):
                    line = line.split('#', 1)[0].rstrip()
                    db.banned_title_content.append(line)
        f.close()

        # load banned body content
        db.banned_body_content      = []
        file_name                   = base_database_path + '/bannedBodyContent.txt'
        with open(file_name) as f:
            list_of_lines = f.read().splitlines()

            for line in list_of_lines:
                if not line.startswith('#'):
                    line = line.split('#', 1)[0].rstrip()
                    db.banned_body_content.append(line)
        f.close()

        # misc file locations
        db.filename_feed = base_database_path + '/cache.' + '.'.join(filter(None, [service_id, bundle_id, env_id])) + '.pickle'


    def debug(self, message, simple=False):
        """
        print debug messages
        """

        if self.getEnv() == 'prod':
            do_debug = False
        else:
            do_debug = True

        if simple and do_debug:
            print '# ' + message
        elif do_debug:
            print '========DEBUG=========\n' + message + '\n====================='


    def error(self, message, exit=False):
        """
        prints error message
        """
        import os

        print '\n========ERROR=========\n' + message + '\n====================='
        if exit:
            os._exit(1)                                                                                                                     # os._exit(1), in comparison to sys.exit(1), exists without throwing an exception : http://stackoverflow.com/questions/173278/is-there-a-way-to-prevent-a-systemexit-exception-raised-from-sys-exit-from-bei


    def errorAsJson(self, message, exit=False):
        """
        prints error message in json format
        """
        import os
        import json

        error_object = {}
        error_object['error'] = message

        print 'Content-Type: application/json; charset="UTF-8"'
        print 'Access-Control-Allow-Origin: *'
        print ''
        print json.dumps(error_object, sort_keys=True, indent=4, separators=(',', ': '), encoding="utf-8")
        if exit:
            os._exit(1)                                                                                                                     # os._exit(1), in comparison to sys.exit(1), exists without throwing an exception : http://stackoverflow.com/questions/173278/is-there-a-way-to-prevent-a-systemexit-exception-raised-from-sys-exit-from-bei


    def getEnv(self):
        """
        return correct environment

        list of valid environments:
        https://github.com/klevstul/foreNeno/wiki/environments
        """
        import os
        import sys

        environment = 'unknownenvironment'

        # check what platform we are running on
        platform = sys.platform

        # check what directory we are running from
        directory = os.path.dirname(os.path.realpath(__file__))

        # the development platform is windows
        if platform == 'win32':
            environment = 'dev'
        # the beta environment has got the name "beta" in the directory path
        elif 'beta' in directory.lower():
            environment = 'beta'
        # the prod environment has got the name "prod" in the directory path
        elif 'prod' in directory.lower():
            environment = 'prod'

        return environment


    def getFeedSpecialCode(self, feed_special_code, code_to_get):
        """
        get and return a feed special code
        """
        return_value = None
        code_to_get = code_to_get.lower() + '='                                             # lower and add the always present '='

        if code_to_get.lower() in feed_special_code.lower():                                # if the code is found...
            length_of_code = len(code_to_get)
            return_value = feed_special_code.lower().encode('utf-8')                        # encode and lower
            return_value = return_value[(return_value.find(code_to_get)+length_of_code):]   # remove everything in front of the code to get
            space_index = return_value.find(' ')                                            # find the first space after the value we are after, in case more special params given
            if space_index < 0: space_index = len(return_value)                             # in case no space (index being -1), then set index to length of string, for avoid loss of last character
            return_value = return_value[:space_index]                                       # remove all after the value

        return return_value


    def htmlToText(self, data):
        """
        http://love-python.blogspot.com/2011/04/html-to-text-in-python.html
        """
        import re

        # remove the newlines
        data    = data.replace('\n', ' ')
        data    = data.replace('\r', ' ')

        # replace consecutive spaces into a single one
        data    = ' '.join(data.split())

        # get only the body content
        #bodyPat = re.compile(r'< body[^<>]*?>(.*?)< / body >', re.I)
        #result = re.findall(bodyPat, data)
        #data = result[0]

        # replace html break tags with spaces
        data    = data.replace('<br />', ' ').replace('<br/>', ' ').replace('<br>', ' ')

        # remove the java script
        p       = re.compile(r'< script[^<>]*?>.*?< / script >')
        data    = p.sub('', data)

        # remove the css styles
        p       = re.compile(r'< style[^<>]*?>.*?< / style >')
        data    = p.sub('', data)

        # remove html comments
        p       = re.compile(r'')
        data    = p.sub('', data)

        # remove all the html tags
        p       = re.compile(r'<[^<]*?>')
        data    = p.sub('', data)

        return data


    def pubDateToNerdDate(self, pubDate, format):
        """
        format the rss pub date (eg. 'Tue, 16 Aug 2016 01:28:47 +0000') to:
        - the sortable 'nerd date' format (eg. '20160816012847')
        - the readable 'display date' format (eg. '2016.08.16 01:28 CET')
        """
        import time
        import feedparser

        # parse it into a standard Python 9-tuple (https://pythonhosted.org/feedparser/date-parsing.html)
        parsed_date = feedparser._parse_date(pubDate)

        # convert python date to what i call "nerd date", the most logical date format of them all (https://docs.python.org/2/library/time.html)
        if (format == 'nerd'):
            parsed_date_string = time.strftime('%Y%m%d%H%M%S', parsed_date)
        # display date, perfect for presenting to the peeps
        elif (format == 'display'):
            parsed_date_string = time.strftime('%Y.%m.%d %H:%M UTC', parsed_date)
        else:
            parsed_date_string = 'unknown_date_format'

        return parsed_date_string


    def asciify(self, text):
        """
        replace special characters with ascii code
        """
        return text.replace(' ','').replace('&aelig;','ae').replace('&oslash;','oe').replace('&aring;','aa').replace('&','').replace(';','').replace(',','').replace('.','').replace('(','').replace(')','').replace('-','').replace('\'','').replace('`','')


    def getQueryString(self):
        """
        returns the entire query string, in other words everything after the '?' on a GET request
        """
        import os

        return os.environ['QUERY_STRING']


    def jsonToDictionary(self, json_data):
        """
        deseralise json into a python object (dictionary)
        ref: https://docs.python.org/2/library/json.html
        """
        import json

        dic = {}

        try:
            dic = json.loads(json_data)
        except ValueError:
            self.error('invalid json format', True)
        except Exception:
            import traceback
            self.error('generic exception: ' + traceback.format_exc(), True)

        return dic


    def loadPickle(self, file_name):
        """
        load a picle on the local os, and return data as dictionary
        """
        import pickle

        return pickle.load(open(file_name, 'rb'))


    def loadWebResource(self, url):
        """
        load a resource on the web, and handle all exceptions
        """
        import urllib2

        try:
            response = urllib2.urlopen(url).read()
        except urllib2.HTTPError, e:
            self.errorAsJson('HTTPError = ' + str(e.code) + '\n' + 'URL = ' + url, True)
        except urllib2.URLError, e:
            self.errorAsJson('URLError = ' + str(e.reason) + '\n' + 'URL = ' + url, True)
        except httplib.HTTPException, e:
            self.errorAsJson('HTTPException' + '\n' + 'URL = ' + url, True)
        except Exception:
            import traceback
            self.errorAsJson('generic exception: ' + traceback.format_exc() + '\n' + 'URL = ' + url, True)

        return response


    def loadFeed(self, url_list):
        """
        go through the list of urls and load the feed
        """
        import feedparser

        # feedparser shouldn't filter <iframe> and HTML5 elements
        try:
            feedparser._HTMLSanitizer.acceptable_elements.add('iframe')
            feedparser._HTMLSanitizer.acceptable_elements.add('article')
            feedparser._HTMLSanitizer.acceptable_elements.add('section')
            feedparser._HTMLSanitizer.acceptable_elements.add('header')
            feedparser._HTMLSanitizer.acceptable_elements.add('footer')
            feedparser._HTMLSanitizer.acceptable_elements.add('aside')
            feedparser._HTMLSanitizer.acceptable_elements.add('audio')
            feedparser._HTMLSanitizer.acceptable_elements.add('video')
        except:
            pass

        return_feed = {}

        for url in url_list:
            # load the feed
            feed = feedparser.parse(url)
            if feed.bozo == 1:
                self.error('bozo_exception [' + str(feed.bozo_exception) + '] for url [' + url + ']', True)

            # either add new entries to existing feed or store a new feed
            if return_feed:
                return_feed['entries'] = return_feed['entries'] + feed.entries
            else:
                return_feed = feed

        return return_feed


if __name__ == '__main__':
    """
    where it all begins
    """

    print 'sorry. can not execute this file. it is a library which has to be included elsewhere.'
