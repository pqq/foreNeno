# -------------------------------------------------------
# author:   Frode Klevstul (frode at klevstul dot com)
# started:  16.03.14
# -------------------------------------------------------

import sys
import pickle
from foreneno import Foreneno


# get an instance of the Foreneno library
foreneno = Foreneno()

# make sure a service id is provided
if len(sys.argv) < 2:
    foreneno.error('missing argument: service_id', True)
service_id = sys.argv[1]

# bundle_id is an optional input parameter
bundle_id = None
if len(sys.argv) > 2:
    bundle_id = sys.argv[2]

# determine the environment
environment_id = foreneno.getEnv()

# initiate the foreneno database given a service_id, environment_id and bundle_id
foreneno.initiateDatabase(foreneno.db, service_id, environment_id, bundle_id)

# load the feed and store it to file, as a pickle
feed = foreneno.loadFeed(foreneno.db.stream_source)
with open(foreneno.db.filename_feed, 'wb') as handle:
    pickle.dump(feed, handle)
    handle.close()
