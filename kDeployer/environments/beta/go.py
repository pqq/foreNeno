# -------------------------------------------------------
# author:   Frode Klevstul (frode at klevstul dot com)
# started:  16.05.15
# -------------------------------------------------------

import re
import sys
import urllib2                      # https://docs.python.org/2/howto/urllib2.html
from foreneno import Foreneno
sys.stderr = sys.stdout


def verifyDestinationUrl(url):
    """
    check url, and if neccessary replace / update the destination url
    """

    # if there are two question marks in the url '?ref' should be changed to '&ref' (issue #397)
    if url.count('?') > 1 and '?ref' in url:
        url = url.replace('?ref', '&ref')

    if '//web.stagram.com/' in url:
        # example url:
        #   https://web.stagram.com/p/BRRS2QCD5Pk?utm_source=websta_rss&utm_campaign=rss?ref=dev.jernvilje.no
        url = url.split('?')[0]
        url = url.replace('web.stagram', 'instagram')

        # old method below (had to load the page, and look for instagram source link)
        #req         = urllib2.Request(url)
        #response    = urllib2.urlopen(req)
        #page        = response.readlines()
        #
        #for line in page:
        #    # looking for this line:
        #    # <li><a href="https://www.instagram.com/p/BFUeOjqSrgK/" target="_blank" rel="nofollow"> View Original on Instagram</a></li>
        #    if 'view original on instagram' in line.lower():
        #        url = re.search("(?P<url>https?://[^\s]+)\"", line).group("url")
        #        #print url
        #        break

    return url


def jumpTo(url):
    """
    jump to web page with url
    """
    print 'Content-Type: text/html; charset="ISO-8859-1"'
    print ''
    print '<html><head><title>foreNeno</title>'
    print '<meta http-equiv="refresh" content="0; url='+url+'">'
    print """
    <!-- google analytics -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-44703456-1', 'auto');
      ga('require', 'linkid');
      ga('send', 'pageview');
    </script>
    """
    print '</head></html>'


def main():
    """
    main, where it all starts...
    """
    foreneno        = Foreneno()
    query_string    = foreneno.getQueryString()
    #query_string    = 'http://websta.me/p/1248755938446981130_8129781'
    #query_string    = 'https://web.stagram.com/p/BRRS2QCD5Pk?utm_source=websta_rss&utm_campaign=rss?ref=dev.jernvilje.no'
    #query_string    = 'http://news.google.com/news/url?sa=t&fd=R&ct2=us&usg=AFQjCNEPvl6gZMKGdKuBnjCJlJLgGTKMqQ&clid=c3a7d30bb8a4878e06b80cf16b898331&cid=52779848421897&ei=hwVMV6DrOsHpygOF5ISIAw&url=http://www.sporten.com/nyhet/disse-skal-kjempe-for-norge-under-ol'
    url             = verifyDestinationUrl(query_string)
    jumpTo(url)


if '__main__' == __name__:
    main()
